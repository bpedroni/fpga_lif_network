
module neuron_core 
   #(parameter 
      ADDRESS_FILTER=1'b0,
      GLOBAL_CELL_BITS=6,
      CELLS=16,
      CELL_BITS=4, 
      SYNAPSE_BITS=2,
      DENDRITE_BITS=2,
      FIFO_WIDTH_BITS=24,
      VMEM_BITS=12)
   
   (
      input rst,
      input clk,
      
      input processCell, 
      input synapseType,


      ///////////////////////
      // DISTANT DOWN FIFO //
      ///////////////////////      
      
      output                      inDistantFIFO_full,
      input [FIFO_WIDTH_BITS-1:0] inDistantFIFO_din,
      input                       inDistantFIFO_wrreq,

      
      /////////////////////
      // DISTANT UP FIFO //
      /////////////////////      
      
      output                       outDistantFIFO_empty,
      input                        outDistantFIFO_rdreq,
      output [FIFO_WIDTH_BITS-1:0] outDistantFIFO_dout
   
   );


// DISTANT IN DENDRITE EVENT FIFO
wire inDistantFIFO_empty, inDistantFIFO_rdreq;
wire [FIFO_WIDTH_BITS-1:0] inDistantFIFO_dout;

wire configRoutingTableFIFO_full, configRoutingTableFIFO_wrreq, configRoutingTableFIFO_empty, configRoutingTableFIFO_rdreq;
wire configWeightFIFO_full, configWeightFIFO_wrreq, configWeightFIFO_empty, configWeightFIFO_rdreq;
wire configCellFIFO_full, configCellFIFO_wrreq, configCellFIFO_empty, configCellFIFO_rdreq;
wire [FIFO_WIDTH_BITS-1:0]   configRoutingTableFIFO_din, configWeightFIFO_din, configCellFIFO_din;
wire [2*FIFO_WIDTH_BITS-1:0] configRoutingTableFIFO_dout, configWeightFIFO_dout, configCellFIFO_dout;


// DISTANT DENDRITE EVENT FIFO: OUTPUT
wire outDistantFIFO_full, outDistantFIFO_wrreq;
wire [FIFO_WIDTH_BITS-1:0] outDistantFIFO_din;
      
// LOCAL DENTRITE EVENT FIFO: ROUTER_OUTGOING -> ROUTER_INCOMING
wire dendLocalFIFO_full, dendLocalFIFO_wrreq, dendLocalFIFO_empty, dendLocalFIFO_rdreq;
wire [CELL_BITS+DENDRITE_BITS-1:0] dendLocalFIFO_din, dendLocalFIFO_dout;

// DENDRITE EVENT FIFO: ROUTER_INCOMING -> SYNAPSE
wire dendriteEventFIFO_full, dendriteEventFIFO_wrreq, dendriteEventFIFO_empty, dendriteEventFIFO_rdreq;
wire [CELL_BITS+DENDRITE_BITS-1:0] dendriteEventFIFO_din, dendriteEventFIFO_dout;

// NEURON REFRACTORY
wire [CELLS-1:0] refractory;
wire [CELLS-1:0] spiked;

// PSP RAM: SYNAPSE <-> NEURON_PROCESSOR
wire [CELL_BITS-1:0] cellPspRAM_wradd, cellPspRAM_rdadd;
wire [VMEM_BITS-1:0] cellPspRAM_din, cellPspRAM_dout;
wire cellPspRAM_wren;

// SPIKE EVENT FIFO: NEURON_PROCESSOR -> ROUTER_OUTGOING
wire spikeEventFIFO_full, spikeEventFIFO_wrreq, spikeEventFIFO_empty, spikeEventFIFO_rdreq;
wire [FIFO_WIDTH_BITS-1:0] spikeEventFIFO_din, spikeEventFIFO_dout;

fifo_24x24 FIFO_DIST_IN_EVENT
	(clk, rst, inDistantFIFO_din, inDistantFIFO_wrreq, inDistantFIFO_rdreq, 
    inDistantFIFO_dout, inDistantFIFO_full, inDistantFIFO_empty);

router_incoming #(CELL_BITS, DENDRITE_BITS, FIFO_WIDTH_BITS)
   ROUTER_INCOMING 
      (rst, clk,
       inDistantFIFO_empty, inDistantFIFO_rdreq, inDistantFIFO_dout,
       dendLocalFIFO_empty, dendLocalFIFO_rdreq, dendLocalFIFO_dout,
       dendriteEventFIFO_full, dendriteEventFIFO_din, dendriteEventFIFO_wrreq,
       configRoutingTableFIFO_full, configRoutingTableFIFO_din, configRoutingTableFIFO_wrreq,
       configWeightFIFO_full, configWeightFIFO_din, configWeightFIFO_wrreq,
       configCellFIFO_full, configCellFIFO_din, configCellFIFO_wrreq);

fifo_24x48 FIFO_CONFIG_ROUTING_TABLE
	(rst, clk, clk, configRoutingTableFIFO_din, configRoutingTableFIFO_wrreq, configRoutingTableFIFO_rdreq,
    configRoutingTableFIFO_dout, configRoutingTableFIFO_full, configRoutingTableFIFO_empty);

fifo_24x48 FIFO_CONFIG_WEIGHTS
	(rst, clk, clk, configWeightFIFO_din, configWeightFIFO_wrreq, configWeightFIFO_rdreq,
    configWeightFIFO_dout, configWeightFIFO_full, configWeightFIFO_empty);

fifo_24x48 FIFO_CONFIG_CELL 
	(rst, clk, clk, configCellFIFO_din, configCellFIFO_wrreq, configCellFIFO_rdreq,
    configCellFIFO_dout, configCellFIFO_full, configCellFIFO_empty);
       
fifo_9x9 FIFO_DEND_EVENT 
	(clk, rst, dendriteEventFIFO_din, dendriteEventFIFO_wrreq, dendriteEventFIFO_rdreq,
    dendriteEventFIFO_dout, dendriteEventFIFO_full, dendriteEventFIFO_empty);

synapse #(CELLS, CELL_BITS, SYNAPSE_BITS, DENDRITE_BITS, VMEM_BITS, FIFO_WIDTH_BITS)
   SYNAPSE
      (rst, clk, processCell, synapseType, 
       dendriteEventFIFO_empty, dendriteEventFIFO_rdreq, dendriteEventFIFO_dout,
       configWeightFIFO_empty, configWeightFIFO_rdreq, configWeightFIFO_dout,
       spiked, refractory,
       cellPspRAM_wradd, cellPspRAM_din, cellPspRAM_wren);

ram_16x12bits PSP_RAM
	(cellPspRAM_wradd, cellPspRAM_din, cellPspRAM_rdadd, clk, cellPspRAM_wren, , cellPspRAM_dout);

neuron_processor #(CELLS, CELL_BITS, VMEM_BITS, FIFO_WIDTH_BITS)
   NEURON_PROCESSOR 
      (rst, clk, processCell, spiked, refractory,
       configCellFIFO_empty, configCellFIFO_rdreq, configCellFIFO_dout,
       cellPspRAM_rdadd, cellPspRAM_dout,
       spikeEventFIFO_full, spikeEventFIFO_din, spikeEventFIFO_wrreq);

fifo_24x24 FIFO_SPIKE_EVENT 
	(clk, rst, spikeEventFIFO_din, spikeEventFIFO_wrreq, spikeEventFIFO_rdreq,
	 spikeEventFIFO_dout, spikeEventFIFO_full, spikeEventFIFO_empty);

router_outgoing #(ADDRESS_FILTER, GLOBAL_CELL_BITS, CELL_BITS, SYNAPSE_BITS, DENDRITE_BITS, VMEM_BITS, FIFO_WIDTH_BITS)
   ROUTER_OUTGOING
      (rst, clk, 
       spikeEventFIFO_empty, spikeEventFIFO_rdreq, spikeEventFIFO_dout,
       configRoutingTableFIFO_empty, configRoutingTableFIFO_rdreq, configRoutingTableFIFO_dout,
       dendLocalFIFO_full, dendLocalFIFO_din, dendLocalFIFO_wrreq,
       outDistantFIFO_full, outDistantFIFO_din, outDistantFIFO_wrreq);

fifo_9x9 FIFO_LOCAL_EVENT 
	(clk, rst, dendLocalFIFO_din, dendLocalFIFO_wrreq, dendLocalFIFO_rdreq,
	 dendLocalFIFO_dout, dendLocalFIFO_full, dendLocalFIFO_empty);

fifo_24x24 FIFO_DIST_UP_EVENT 
	(clk, rst, outDistantFIFO_din, outDistantFIFO_wrreq, outDistantFIFO_rdreq,
	 outDistantFIFO_dout, outDistantFIFO_full, outDistantFIFO_empty);

endmodule
   