
module synapse 
   #(parameter 
      CELLS=64,
      CELL_BITS=6, 
      SYNAPSE_BITS=4,
      DENDRITE_BITS=4,
      VMEM_BITS=12,
      FIFO_WIDTH_BITS=24)
   (
      input rst,
      input clk,
      
      input processCell,
      input synapseType,
      
      
      /////////////////////////
      // DENTRITE EVENT FIFO //
      /////////////////////////
      
      input                               inDendriteEventFIFO_empty,
      output reg                          inDendriteEventFIFO_rdreq,
      input [CELL_BITS+DENDRITE_BITS-1:0] inDendriteEventFIFO_dout,
      
      
      /////////////////////////////////////
      // WEIGHT CONFIGURATION EVENT FIFO //
      /////////////////////////////////////
      
      input                          configWeightFIFO_empty,
      output reg                     configWeightFIFO_rdreq,
      input [2*FIFO_WIDTH_BITS-1:0]  configWeightFIFO_dout,
      
      
      ///////////////////////////////////////
      // NEURON SPIKE AND REFRACTORY FLAGS //
      ///////////////////////////////////////
      
      input [CELLS-1:0] spiked,                  // will be used for STDP
      input [CELLS-1:0] refractory,
      
      
      /////////////
      // PSP RAM //
      /////////////
      
      output [CELL_BITS-1:0] cellPspRAM_wradd,
      output [VMEM_BITS-1:0] cellPspRAM_din,
      output reg             cellPspRAM_wren

   );
   

parameter WEIGHT_BITS = VMEM_BITS;

   
// Process dendrite event / pre-PSP->PSP transfer
reg                  move_to_next_cell;
reg [CELL_BITS-1:0]  cell_being_processed;
reg                  select_transfer;           // used to indicate if processing dendrite event (0) or pre-PSP->PSP transfer (1)
wire [CELL_BITS-1:0] dendrite_event_cell = inDendriteEventFIFO_dout[CELL_BITS+DENDRITE_BITS-1:DENDRITE_BITS];
   
   
////////////////
// Weight RAM //
////////////////

wire [CELL_BITS-1:0]     config_weight_cell     = configWeightFIFO_dout[31+CELL_BITS:32];
wire [DENDRITE_BITS-1:0] config_weight_dendrite = configWeightFIFO_dout[23+DENDRITE_BITS:24];
wire [WEIGHT_BITS-1:0]   config_weight_value    = configWeightFIFO_dout[WEIGHT_BITS-1:0];

wire [WEIGHT_BITS-1:0] weightRAM_din = config_weight_value;
wire [CELL_BITS+DENDRITE_BITS-1:0] weightRAM_wradd = {config_weight_cell, config_weight_dendrite};
reg weightRAM_wren;

wire [CELL_BITS+DENDRITE_BITS-1:0] weightRAM_rdadd;
wire signed [WEIGHT_BITS-1:0] weightRAM_dout;

ram_512x12bits WEIGHT_RAM
   (weightRAM_wradd, weightRAM_din, weightRAM_rdadd, clk, weightRAM_wren, , weightRAM_dout);


////////////////////  
// Spike time RAM //
////////////////////

//wire [SPIKE_TIME_RAM_WIDTH_BITS-1:0] spikeTimeRAM_din, spikeTimeRAM_dout;
//wire [SPIKE_TIME_RAM_DEPTH_BITS-1:0] spikeTimeRAM_wradd, spikeTimeRAM_rdadd;
//reg spikeTimeRAM_wren;

//ip_ddr #(SPIKE_TIME_RAM_DEPTH_BITS, SPIKE_TIME_RAM_WIDTH_BITS) SPIKE_TIME_RAM
//   (rst, clk, spikeTimeRAM_din, spikeTimeRAM_rdadd, spikeTimeRAM_wradd, spikeTimeRAM_wren, spikeTimeRAM_dout);


/////////////////
// Pre-PSP RAM //
/////////////////

wire signed [VMEM_BITS-1:0] precellPspRAM_din, precellPspRAM_dout;
wire [CELL_BITS-1:0] precellPspRAM_wradd, precellPspRAM_rdadd;
reg precellPspRAM_wren;

ram_16x12bits PRE_PSP_RAM
   (precellPspRAM_wradd, precellPspRAM_din, precellPspRAM_rdadd, clk, precellPspRAM_wren, , precellPspRAM_dout);

   
////////////////////////////////////////////////////////////////////////////////////
// State machine: servicing global 'tick', configuration event, and dendrite event
parameter [3:0] STATE_RESET                    = 4'd00;
parameter [3:0] STATE_IDLE                     = 4'd01;
parameter [3:0] STATE_TRANSFER_PRE_PSP1        = 4'd02;
parameter [3:0] STATE_TRANSFER_PRE_PSP2        = 4'd03;
parameter [3:0] STATE_VERIFY_SYNAPSE_TYPE      = 4'd04;
parameter [3:0] STATE_WAIT_PRE_PSP_RAM1        = 4'd05;
parameter [3:0] STATE_WAIT_PRE_PSP_RAM2        = 4'd06;
parameter [3:0] STATE_CONFIG_WEIGHT            = 4'd07;
parameter [3:0] STATE_VERIFY_REFRACTORY        = 4'd08;
parameter [3:0] STATE_READ_PRE_PSP_AND_WEIGHT1 = 4'd09;
parameter [3:0] STATE_READ_PRE_PSP_AND_WEIGHT2 = 4'd10;

reg [3:0] curr_state;
reg [3:0] next_state;

always @(posedge rst or posedge clk)
begin
   if(rst)
      curr_state <= STATE_RESET;
   else
      curr_state <= next_state;
end

always @(*)
begin

   select_transfer           = 0;
   configWeightFIFO_rdreq    = 0;
   inDendriteEventFIFO_rdreq = 0;
   cellPspRAM_wren           = 0;
   move_to_next_cell         = 0;
   precellPspRAM_wren        = 0;
   weightRAM_wren            = 0;
   
   next_state = curr_state;

   case(curr_state)
      STATE_RESET:
      begin
         next_state = STATE_IDLE;
      end
      
      STATE_IDLE:
      begin
         // global clock 'tick': transfer pre-PSP->PSP
         if(processCell)begin
            select_transfer = 1;
            next_state = STATE_TRANSFER_PRE_PSP1;

         // process weight configuration event
         end else if(~configWeightFIFO_empty)begin
            configWeightFIFO_rdreq = 1;
            next_state = STATE_CONFIG_WEIGHT;
         
         // process dendrite event
         end else if(~inDendriteEventFIFO_empty)begin
            select_transfer = 0;
            inDendriteEventFIFO_rdreq = 1;
            next_state = STATE_VERIFY_REFRACTORY;
         end
      end
      
      //////////////////////////////////////////
      // two clock cycles to read pre-PSP RAM
      STATE_TRANSFER_PRE_PSP1:
      begin
         select_transfer = 1;
         next_state = STATE_TRANSFER_PRE_PSP2;
      end
      STATE_TRANSFER_PRE_PSP2:
      begin
         select_transfer = 1;
         cellPspRAM_wren = 1;
         next_state = STATE_VERIFY_SYNAPSE_TYPE;
      end
      STATE_VERIFY_SYNAPSE_TYPE:
      begin
         select_transfer = 1;
         
         if(~synapseType)
            precellPspRAM_wren = 1;

         if(cell_being_processed < ((1<<CELL_BITS)-1))begin
            move_to_next_cell = 1;
            next_state = STATE_WAIT_PRE_PSP_RAM1;
         end else begin
            move_to_next_cell = 0;
            next_state = STATE_IDLE;
         end
      end
      STATE_WAIT_PRE_PSP_RAM1:
      begin
         select_transfer = 1;
         next_state = STATE_WAIT_PRE_PSP_RAM2;
      end
      STATE_WAIT_PRE_PSP_RAM2:
      begin
         select_transfer = 1;
         next_state = STATE_TRANSFER_PRE_PSP1;
      end

      //////////////////
      // update weight 
      STATE_CONFIG_WEIGHT:
      begin
         weightRAM_wren = 1;
         next_state = STATE_IDLE;
      end
      
      /////////////////////////////////////////////
      // verify if cell is not inside refractory
      STATE_VERIFY_REFRACTORY:
      begin
         if(refractory[dendrite_event_cell])
            next_state = STATE_IDLE;
         else
            next_state = STATE_READ_PRE_PSP_AND_WEIGHT1;
      end
      
      // Fetch pre-PSP and weight
      STATE_READ_PRE_PSP_AND_WEIGHT1:
      begin
         next_state = STATE_READ_PRE_PSP_AND_WEIGHT2;
      end
      STATE_READ_PRE_PSP_AND_WEIGHT2:
      begin
         precellPspRAM_wren = 1;
         next_state = STATE_IDLE;
      end
      
      default:
      begin
         select_transfer           = 0;
         configWeightFIFO_rdreq    = 0;
         inDendriteEventFIFO_rdreq = 0;
         cellPspRAM_wren           = 0;
         move_to_next_cell         = 0;
         precellPspRAM_wren        = 0;
         weightRAM_wren            = 0;
         next_state = STATE_RESET;
      end
   endcase
end


// write pre-PSP during transfer phase (select_transfer) and during pre-PSP update phase (dendrite event processing)
wire signed [WEIGHT_BITS:0] new_weight_overflow = precellPspRAM_dout + weightRAM_dout;

reg [WEIGHT_BITS-1:0] new_weight;
always @(*)
begin
   if(new_weight_overflow > ((1<<(WEIGHT_BITS-1))-1))
      new_weight = ((1<<(WEIGHT_BITS-1))-1);
   else if(new_weight_overflow < -(1<<(WEIGHT_BITS-1)))
      new_weight = -(1<<(WEIGHT_BITS-1));
   else
      new_weight = new_weight_overflow[WEIGHT_BITS-1:0];
end

assign precellPspRAM_wradd = select_transfer ? cell_being_processed : dendrite_event_cell;
assign precellPspRAM_din = select_transfer ? 0 : new_weight;

// read pre-PSP during transfer phase (select_transfer) and during pre-PSP update phase (dendrite event processing)
assign precellPspRAM_rdadd = select_transfer ? cell_being_processed : dendrite_event_cell;
assign weightRAM_rdadd = inDendriteEventFIFO_dout;

assign cellPspRAM_wradd = cell_being_processed;
assign cellPspRAM_din   = precellPspRAM_dout;


// cycle through neuron cells to transfer prePSP -> PSP
always @(posedge rst or posedge clk)
begin
   if(rst)
      cell_being_processed <= 0;
   else if(move_to_next_cell)
      cell_being_processed <= cell_being_processed + 1;
   else if(curr_state == STATE_IDLE)
      cell_being_processed <= 0;
end

endmodule

