
module router_outgoing
   #(parameter 
      ADDRESS_FILTER=4'd0,
      GLOBAL_CELL_BITS=10,
      CELL_BITS=6,
      SYNAPSE_BITS=6,
      DENDRITE_BITS=6,
      VMEM_BITS=12,
      FIFO_WIDTH_BITS=24)
   (
      input rst,
      input clk,
   
      /////////////////////
      // INCOMING EVENTS //
      /////////////////////
      
      // Spike event
      input                         inSpikeEventFIFO_empty,
      output reg                    inSpikeEventFIFO_rdreq,
      input [FIFO_WIDTH_BITS-1:0]   inSpikeEventFIFO_dout,
      
      // Routing table configuration event
      input                         configRoutingTableFIFO_empty,
      output reg                    configRoutingTableFIFO_rdreq,
      input [2*FIFO_WIDTH_BITS-1:0] configRoutingTableFIFO_dout,
      
      
      /////////////////////
      // OUTGOING EVENTS //
      /////////////////////
      
      // Local (dendrite) event
      input                                outLocalFIFO_full,
      output [CELL_BITS+DENDRITE_BITS-1:0] outLocalFIFO_din,
      output reg                           outLocalFIFO_wrreq,

      // Distant event
      input                                outDistantFIFO_full,
      output reg [FIFO_WIDTH_BITS-1:0]     outDistantFIFO_din,
      output reg                           outDistantFIFO_wrreq
      
   );


// synapse defines the second part of the routing table RAM address
reg [SYNAPSE_BITS-1:0] synapse;
reg move_to_next_synapse;


//////////////////////////////////////////////////////////////////////////
// RAM read address defined by current neuron cell and synapses addresses
wire [CELL_BITS+SYNAPSE_BITS-1:0] routingRAM_rdadd = {inSpikeEventFIFO_dout[CELL_BITS-1:0], synapse};
wire [GLOBAL_CELL_BITS+DENDRITE_BITS-1:0] routingRAM_dout;

wire [SYNAPSE_BITS-1:0] routing_table_synapse  = configRoutingTableFIFO_dout[23+SYNAPSE_BITS:24];
wire [CELL_BITS-1:0] routing_table_cell_origin = configRoutingTableFIFO_dout[31+CELL_BITS:32];
wire [DENDRITE_BITS-1:0] routing_table_dendrite            = configRoutingTableFIFO_dout[DENDRITE_BITS-1:0];
wire [GLOBAL_CELL_BITS-1:0] routing_table_cell_destination = configRoutingTableFIFO_dout[7+GLOBAL_CELL_BITS:8];

reg                                       routingRAM_wren;
wire [CELL_BITS+SYNAPSE_BITS-1:0]         routingRAM_wradd = {routing_table_cell_origin, routing_table_synapse};
wire [GLOBAL_CELL_BITS+DENDRITE_BITS-1:0] routingRAM_din   = {routing_table_cell_destination, routing_table_dendrite};

ram_512x13bits ROUTING_RAM
   (routingRAM_wradd, routingRAM_din, routingRAM_rdadd, clk, routingRAM_wren, , routingRAM_dout);

  
////////////////////////////////////////////////////////////////
// address_filter defines if is local or distant dendrite event
wire [GLOBAL_CELL_BITS-CELL_BITS-1:0] address_filter = routingRAM_dout[GLOBAL_CELL_BITS+DENDRITE_BITS-1:CELL_BITS+DENDRITE_BITS];

// local events (side)
assign outLocalFIFO_din = routingRAM_dout[CELL_BITS+DENDRITE_BITS-1:0];

// distant events (up)
wire [1:0] out_distant_event_code = inSpikeEventFIFO_dout[FIFO_WIDTH_BITS-1:FIFO_WIDTH_BITS-2];

wire [FIFO_WIDTH_BITS-1:0] out_distant_dendrite_event;
assign out_distant_dendrite_event[FIFO_WIDTH_BITS-1:GLOBAL_CELL_BITS+DENDRITE_BITS] = 0;
assign out_distant_dendrite_event[GLOBAL_CELL_BITS+DENDRITE_BITS-1:0] = routingRAM_dout;

wire [FIFO_WIDTH_BITS-1:0] out_distant_spike_to_usb;
assign out_distant_spike_to_usb[FIFO_WIDTH_BITS-1:FIFO_WIDTH_BITS-2] = 2'b10;
assign out_distant_spike_to_usb[FIFO_WIDTH_BITS-3:GLOBAL_CELL_BITS] = 0;
assign out_distant_spike_to_usb[GLOBAL_CELL_BITS-1:0] = {ADDRESS_FILTER, inSpikeEventFIFO_dout[CELL_BITS-1:0]};

wire [FIFO_WIDTH_BITS-1:0] out_distant_Vmem_to_usb;
assign out_distant_Vmem_to_usb[FIFO_WIDTH_BITS-1:FIFO_WIDTH_BITS-2] = 2'b11;
assign out_distant_Vmem_to_usb[FIFO_WIDTH_BITS-3:GLOBAL_CELL_BITS] = 0;
assign out_distant_Vmem_to_usb[GLOBAL_CELL_BITS-1:0] = {ADDRESS_FILTER, inSpikeEventFIFO_dout[CELL_BITS-1:0]};

reg [1:0] distant_event_select;
always @(*)
begin
   case(distant_event_select)
      0: outDistantFIFO_din = out_distant_dendrite_event;   // dendrite event
      1: outDistantFIFO_din = out_distant_spike_to_usb;     // to USB: spike cell address
      2: outDistantFIFO_din = out_distant_Vmem_to_usb;      // to USB: Vmem cell address (part 1/2)
      3: outDistantFIFO_din = inSpikeEventFIFO_dout;        // to USB: Vmem value (part 2/2)
   endcase
end


/////////////////////
// State machine

parameter [3:0] STATE_RESET                = 4'd00;
parameter [3:0] STATE_IDLE                 = 4'd01;
parameter [3:0] STATE_CONFIG_ROUTING_TABLE = 4'd02;
parameter [3:0] STATE_VERIFY_SPIKE_EVENT   = 4'd03;
parameter [3:0] STATE_READ_ROUTING_RAM1    = 4'd04;
parameter [3:0] STATE_READ_ROUTING_RAM2    = 4'd05;
parameter [3:0] STATE_READ_ROUTING_RAM3    = 4'd06;
parameter [3:0] STATE_ROUTE_LOCAL          = 4'd07;
parameter [3:0] STATE_ROUTE_DISTANT        = 4'd08;
parameter [3:0] STATE_NEXT_SYNAPSE         = 4'd09;
parameter [3:0] STATE_TX_VMEM_VALUE1       = 4'd10;
parameter [3:0] STATE_TX_VMEM_VALUE2       = 4'd11;

reg [3:0] curr_state;
reg [3:0] next_state;

always @(posedge rst or posedge clk)
begin
   if(rst)
      curr_state <= STATE_RESET;
   else
      curr_state <= next_state;
end

always @(*)
begin
   
   configRoutingTableFIFO_rdreq = 0;
   routingRAM_wren              = 0;
   inSpikeEventFIFO_rdreq       = 0;
   outLocalFIFO_wrreq           = 0;
   outDistantFIFO_wrreq         = 0;
   move_to_next_synapse         = 0;
   distant_event_select         = 0;
   
   next_state = curr_state;

   case(curr_state)
      STATE_RESET:
      begin
         next_state = STATE_IDLE;
      end
      
      // wait for incoming spike event
      STATE_IDLE:
      begin
         if(~configRoutingTableFIFO_empty)begin
            configRoutingTableFIFO_rdreq = 1;
            next_state = STATE_CONFIG_ROUTING_TABLE;
         end else if(~inSpikeEventFIFO_empty)begin
            inSpikeEventFIFO_rdreq = 1;
            next_state = STATE_VERIFY_SPIKE_EVENT;
         end
      end

      // update routing table
      STATE_CONFIG_ROUTING_TABLE:
      begin
         routingRAM_wren = 1;
         next_state = STATE_IDLE;
      end
      
      // verify spike event
      STATE_VERIFY_SPIKE_EVENT:
      begin
         if(~outDistantFIFO_full)begin
            if(out_distant_event_code == 2'b00)begin
               next_state = STATE_READ_ROUTING_RAM1;
            end else if(out_distant_event_code == 2'b10)begin
               distant_event_select = 1;
               next_state = STATE_IDLE;
            end else begin
               distant_event_select = 2;
               next_state = STATE_TX_VMEM_VALUE1;
            end

            outDistantFIFO_wrreq = 1;
         end
      end

      STATE_TX_VMEM_VALUE1:
      begin
         if(~inSpikeEventFIFO_empty)begin
            inSpikeEventFIFO_rdreq = 1;
            next_state = STATE_TX_VMEM_VALUE2;
         end
      end
      STATE_TX_VMEM_VALUE2:
      begin
         if(~outDistantFIFO_full)begin
            distant_event_select = 3;
            outDistantFIFO_wrreq = 1;
            next_state = STATE_IDLE;
         end
      end
            
      // two clock cycles to read routing table RAM
      STATE_READ_ROUTING_RAM1:
      begin
         next_state = STATE_READ_ROUTING_RAM2;
      end
      STATE_READ_ROUTING_RAM2:
      begin
         next_state = STATE_READ_ROUTING_RAM3;
      end
      
      // verify if is local or distant dendrite event
      STATE_READ_ROUTING_RAM3:
      begin
         if(routingRAM_dout != 0)begin
            if(address_filter == ADDRESS_FILTER)
               next_state = STATE_ROUTE_LOCAL;
            else
               next_state = STATE_ROUTE_DISTANT;
         end else
            next_state = STATE_IDLE;
      end
      
      // write to local dendrite event FIFO and move on to next synapse
      STATE_ROUTE_LOCAL:
      begin
         if(~outLocalFIFO_full)begin
            outLocalFIFO_wrreq = 1;
            next_state = STATE_NEXT_SYNAPSE;
         end
      end
      
      // write to distant dendrite event FIFO and move on to next synapse
      STATE_ROUTE_DISTANT:
      begin
         distant_event_select = 0;
         if(~outDistantFIFO_full)begin
            outDistantFIFO_wrreq = 1;
            next_state = STATE_NEXT_SYNAPSE;
         end
      end
      
      // verify if has not reached synapse limit
      STATE_NEXT_SYNAPSE:
      begin
         if(synapse < ((1<<SYNAPSE_BITS)-1))begin
            move_to_next_synapse = 1;
            next_state = STATE_READ_ROUTING_RAM1;
         end else begin
            move_to_next_synapse = 0;
            next_state = STATE_IDLE;
         end
      end
      
      default:
      begin
         configRoutingTableFIFO_rdreq = 0;
         routingRAM_wren              = 0;
         inSpikeEventFIFO_rdreq       = 0;
         outLocalFIFO_wrreq           = 0;
         outDistantFIFO_wrreq         = 0;
         move_to_next_synapse         = 0;
         distant_event_select         = 0;
         
         next_state = STATE_RESET;
      end
   endcase
end

// cycle through synapses; hault while servicing
always @(posedge rst or posedge clk)
begin
   if(rst)
      synapse <= 0;
   else
   begin
      if(curr_state == STATE_IDLE)
         synapse <= 0;
      else if(move_to_next_synapse)
         synapse <= synapse + 1;
   end
end

endmodule
