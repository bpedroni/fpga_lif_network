
module router_incoming 
   #(parameter 
      CELL_BITS=4, 
      DENDRITE_BITS=4,
      FIFO_WIDTH_BITS=24)
   (
      input rst,
      input clk,
      
     
      /////////////////////////
      // INCOMING EVENT FIFO //
      /////////////////////////
      
      // Distant
      input                       inDistantFIFO_empty,
      output reg                  inDistantFIFO_rdreq,
      input [FIFO_WIDTH_BITS-1:0] inDistantFIFO_dout,
      
      // Local
      input                               inLocalFIFO_empty,
      output reg                          inLocalFIFO_rdreq,
      input [CELL_BITS+DENDRITE_BITS-1:0] inLocalFIFO_dout,
      

      /////////////////////////////
      // OUTGOING DENDRITE EVENT //
      /////////////////////////////
      
      input                                outDendriteEventFIFO_full,
      output [CELL_BITS+DENDRITE_BITS-1:0] outDendriteEventFIFO_din,
      output reg                           outDendriteEventFIFO_wrreq,
      

      //////////////////////////////////
      // OUTGOING CONFIGURATION EVENT //
      //////////////////////////////////
    
      // Routing table
      input                          configRoutingTableFIFO_full,
      output [FIFO_WIDTH_BITS-1:0]   configRoutingTableFIFO_din,
      output reg                     configRoutingTableFIFO_wrreq,
    
      // Dendrite weights
      input                          configWeightFIFO_full,
      output [FIFO_WIDTH_BITS-1:0]   configWeightFIFO_din,
      output reg                     configWeightFIFO_wrreq,
      
      // Neuron cell parameters
      input                          configCellFIFO_full,
      output [FIFO_WIDTH_BITS-1:0]   configCellFIFO_din,
      output reg                     configCellFIFO_wrreq

      
   );


// Configuration event processing
assign configRoutingTableFIFO_din = inDistantFIFO_dout;
assign configWeightFIFO_din       = inDistantFIFO_dout;
assign configCellFIFO_din         = inDistantFIFO_dout;

// Dendrite event processing
reg distant_event;
wire [CELL_BITS-1:0]     distant_event_cell = inDistantFIFO_dout[7+CELL_BITS:8];
wire [DENDRITE_BITS-1:0] distant_event_dend = inDistantFIFO_dout[DENDRITE_BITS-1:0];
wire [CELL_BITS+DENDRITE_BITS-1:0] distant_event_address = {distant_event_cell, distant_event_dend};
assign outDendriteEventFIFO_din = distant_event ?  distant_event_address : inLocalFIFO_dout;

// State machine: servicing dendrite event
parameter [3:0] STATE_RESET                 = 4'd00;
parameter [3:0] STATE_IDLE                  = 4'd01;
parameter [3:0] STATE_DISTANT_EVENT         = 4'd02;
parameter [3:0] STATE_CONFIG_ROUTING_TABLE1 = 4'd03;
parameter [3:0] STATE_CONFIG_ROUTING_TABLE2 = 4'd04;
parameter [3:0] STATE_CONFIG_ROUTING_TABLE3 = 4'd05;
parameter [3:0] STATE_CONFIG_WEIGHT1        = 4'd06;
parameter [3:0] STATE_CONFIG_WEIGHT2        = 4'd07;
parameter [3:0] STATE_CONFIG_WEIGHT3        = 4'd08;
parameter [3:0] STATE_CONFIG_CELL1          = 4'd09;
parameter [3:0] STATE_CONFIG_CELL2          = 4'd10;
parameter [3:0] STATE_CONFIG_CELL3          = 4'd11;
parameter [3:0] STATE_DENDRITE_EVENT        = 4'd12;

reg [3:0] curr_state;
reg [3:0] next_state;

always @(posedge rst or posedge clk)
begin
   if(rst)
      curr_state <= STATE_RESET;
   else
      curr_state <= next_state;
end

always @(*)
begin

   inDistantFIFO_rdreq          = 0;
   inLocalFIFO_rdreq            = 0;
   configRoutingTableFIFO_wrreq = 0;
   configWeightFIFO_wrreq       = 0;
   configCellFIFO_wrreq         = 0;
   outDendriteEventFIFO_wrreq   = 0;
   
   next_state = curr_state;

   case(curr_state)
      STATE_RESET:
      begin
         next_state = STATE_IDLE;
      end
      
      // wait for incoming event
      STATE_IDLE:
      begin
         if(~inDistantFIFO_empty)begin
            inDistantFIFO_rdreq = 1;
            next_state = STATE_DISTANT_EVENT;
         end else if(~inLocalFIFO_empty)begin
            inLocalFIFO_rdreq = 1;
            next_state = STATE_DENDRITE_EVENT;
         end
      end
      
      // verify if is configuration or dendrite event
      STATE_DISTANT_EVENT:
      begin
         if(inDistantFIFO_dout[FIFO_WIDTH_BITS-1])begin
            if(inDistantFIFO_dout[FIFO_WIDTH_BITS-2:FIFO_WIDTH_BITS-3] == 2'b00)
               next_state = STATE_CONFIG_ROUTING_TABLE1;
            else if(inDistantFIFO_dout[FIFO_WIDTH_BITS-2:FIFO_WIDTH_BITS-3] == 2'b01)
               next_state = STATE_CONFIG_WEIGHT1;
            else
               next_state = STATE_CONFIG_CELL1;
         end else
            next_state = STATE_DENDRITE_EVENT;
      end
      
      ////////////////////////////////////
      STATE_CONFIG_ROUTING_TABLE1:
      begin
         if(~configRoutingTableFIFO_full)begin
            configRoutingTableFIFO_wrreq = 1;
            next_state = STATE_CONFIG_ROUTING_TABLE2;
         end
      end
      STATE_CONFIG_ROUTING_TABLE2:
      begin
         if(~inDistantFIFO_empty)begin
            inDistantFIFO_rdreq = 1;
            next_state = STATE_CONFIG_ROUTING_TABLE3;
         end
      end
      STATE_CONFIG_ROUTING_TABLE3:
      begin
         if(~configRoutingTableFIFO_full)begin
            configRoutingTableFIFO_wrreq = 1;
            next_state = STATE_IDLE;
         end
      end
      
      ////////////////////////////////////
      STATE_CONFIG_WEIGHT1:
      begin
         if(~configWeightFIFO_full)begin
            configWeightFIFO_wrreq = 1;
            next_state = STATE_CONFIG_WEIGHT2;
         end
      end
      STATE_CONFIG_WEIGHT2:
      begin
         if(~inDistantFIFO_empty)begin
            inDistantFIFO_rdreq = 1;
            next_state = STATE_CONFIG_WEIGHT3;
         end
      end
      STATE_CONFIG_WEIGHT3:
      begin
         if(~configWeightFIFO_full)begin
            configWeightFIFO_wrreq = 1;
            next_state = STATE_IDLE;
         end
      end
      
      ////////////////////////////////////
      STATE_CONFIG_CELL1:
      begin
         if(~configCellFIFO_full)begin
            configCellFIFO_wrreq = 1;
            next_state = STATE_CONFIG_CELL2;
         end
      end
      STATE_CONFIG_CELL2:
      begin
         if(~inDistantFIFO_empty)begin
            inDistantFIFO_rdreq = 1;
            next_state = STATE_CONFIG_CELL3;
         end
      end
      STATE_CONFIG_CELL3:
      begin
         if(~configCellFIFO_full)begin
            configCellFIFO_wrreq = 1;
            next_state = STATE_IDLE;
         end
      end
      
      ////////////////////////////////////
      STATE_DENDRITE_EVENT:
      begin
         if(~outDendriteEventFIFO_full)begin
            outDendriteEventFIFO_wrreq = 1;
            next_state = STATE_IDLE;
         end
      end
      
      default:
      begin
         inDistantFIFO_rdreq          = 0;
         inLocalFIFO_rdreq            = 0;
         configRoutingTableFIFO_wrreq = 0;
         configWeightFIFO_wrreq       = 0;
         configCellFIFO_wrreq         = 0;
         outDendriteEventFIFO_wrreq   = 0;
         next_state = STATE_RESET;
      end
   endcase
end

// control MUX for distant/local event
always @(posedge rst or posedge clk)
begin
   if(rst)
      distant_event <= 0;
   else if(curr_state == STATE_DISTANT_EVENT)
      distant_event <= 1;
   else if(curr_state == STATE_IDLE)
      distant_event <= 0;
end

endmodule

