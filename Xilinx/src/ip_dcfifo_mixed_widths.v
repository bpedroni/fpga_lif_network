module ip_dcfifo_mixed_widths #(parameter DEPTH_BITS=5, IN_WIDTH_BITS=8, OUT_WIDTH_BITS=16) (
	aclr,
	data,
	rdclk,
	rdreq,
	wrclk,
	wrreq,
	q,
	rdempty,
	wrfull);

	input	  aclr;
	input	[IN_WIDTH_BITS-1:0]  data;
	input	  rdclk;
	input	  rdreq;
	input	  wrclk;
	input	  wrreq;
	output	[OUT_WIDTH_BITS-1:0]  q;
	output	  rdempty;
	output	  wrfull;

	tri0	  aclr;

	wire [OUT_WIDTH_BITS-1:0] sub_wire0;
	wire  sub_wire1;
	wire  sub_wire2;
	wire [OUT_WIDTH_BITS-1:0] q = sub_wire0[OUT_WIDTH_BITS-1:0];
	wire  rdempty = sub_wire1;
	wire  wrfull = sub_wire2;

	dcfifo_mixed_widths	dcfifo_mixed_widths_component (
				.aclr (aclr),
				.data (data),
				.rdclk (rdclk),
				.rdreq (rdreq),
				.wrclk (wrclk),
				.wrreq (wrreq),
				.q (sub_wire0),
				.rdempty (sub_wire1),
				.wrfull (sub_wire2),
				.rdfull (),
				.rdusedw (),
				.wrempty (),
				.wrusedw ());
	defparam
		dcfifo_mixed_widths_component.intended_device_family = "Cyclone IV E",
		dcfifo_mixed_widths_component.lpm_numwords = 1 << DEPTH_BITS,
		dcfifo_mixed_widths_component.lpm_showahead = "OFF",
		dcfifo_mixed_widths_component.lpm_type = "dcfifo_mixed_widths",
		dcfifo_mixed_widths_component.lpm_width = IN_WIDTH_BITS,
		dcfifo_mixed_widths_component.lpm_widthu = DEPTH_BITS,
		dcfifo_mixed_widths_component.lpm_widthu_r = DEPTH_BITS-OUT_WIDTH_BITS/IN_WIDTH_BITS+1,
		dcfifo_mixed_widths_component.lpm_width_r = OUT_WIDTH_BITS,
		dcfifo_mixed_widths_component.overflow_checking = "OFF",
		dcfifo_mixed_widths_component.rdsync_delaypipe = 4,
		dcfifo_mixed_widths_component.read_aclr_synch = "OFF",
		dcfifo_mixed_widths_component.underflow_checking = "OFF",
		dcfifo_mixed_widths_component.use_eab = "ON",
		dcfifo_mixed_widths_component.write_aclr_synch = "OFF",
		dcfifo_mixed_widths_component.wrsync_delaypipe = 4;

endmodule
