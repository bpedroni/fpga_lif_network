
module usb_arbiter_interface
   #(parameter 
      USB_WIDTH_BITS=8,
      FIFO_DISTANT_WIDTH_BITS=24) 
   (
      input rst,
      input clk,

      output reg processCell_en,
      
      ///////////////////////
      // INCOMING USB PATH //
      ///////////////////////
      
		input											 usbCtrlInFIFO_empty,		// from usb_fifo_control to interface
      output reg                       	 usbCtrlInFIFO_rdreq,
		input [USB_WIDTH_BITS-1:0]       	 usbCtrlInFIFO_dout,

      output                           	 inUsbFIFO_empty,				// from interface to arbiter_top
      input                            	 inUsbFIFO_rdreq,
      output [FIFO_DISTANT_WIDTH_BITS-1:0] inUsbFIFO_dout,
      
      
      ///////////////////////
      // OUTGOING USB PATH //
      ///////////////////////
      
      output                          		outUsbFIFO_full,				// from arbiter_top to interface
      input [FIFO_DISTANT_WIDTH_BITS-1:0] outUsbFIFO_din,
      input                           	   outUsbFIFO_wrreq,

      input                           	   usbCtrlOutFIFO_full,			// from interface to usb_fifo_control
		output [USB_WIDTH_BITS-1:0]     	   usbCtrlOutFIFO_din,			
      output reg                      	   usbCtrlOutFIFO_wrreq
      
   );


always @(posedge rst or posedge clk)
begin
   if(rst)
      processCell_en <= 1'b0;
   else if(usbCtrlInFIFO_dout[7:1] == 7'b1111111)
      processCell_en <= usbCtrlInFIFO_dout[0];
end
   
///////////////
// USB FIFOs //
///////////////

reg inUsbFIFO_wrreq;
wire inUsbFIFO_full;

ip_fifo_usb_in USB_FIFO_IN 
   (rst, clk, clk, usbCtrlInFIFO_dout, inUsbFIFO_wrreq, inUsbFIFO_rdreq, inUsbFIFO_dout, inUsbFIFO_full, inUsbFIFO_empty);

reg outUsbFIFO_rdreq;
wire outUsbFIFO_empty;

ip_fifo_usb_out USB_FIFO_OUT
   (rst, clk, clk, outUsbFIFO_din, outUsbFIFO_wrreq, outUsbFIFO_rdreq, usbCtrlOutFIFO_din, outUsbFIFO_full, outUsbFIFO_empty);


////////////////////////////////////
// USB RECEIVE DATA STATE MACHINE //
////////////////////////////////////

reg [1:0] curr_state;
reg [1:0] next_state;

parameter [1:0] STATE_RESET                               = 2'd0;
parameter [1:0] STATE_IDLE                                = 2'd1;
parameter [1:0] STATE_VERIFY_SYSTEM_PROCESSING_ACTIVATION = 2'd2;
parameter [1:0] STATE_RX_USB_DATA                         = 2'd3;

always @(posedge rst or posedge clk)
begin
	if(rst)
		curr_state <= STATE_RESET;
   else
		curr_state <= next_state;
end

always @(*)
begin
   usbCtrlInFIFO_rdreq = 0;
   inUsbFIFO_wrreq     = 0;
   
   next_state = curr_state;

	case(curr_state)
		STATE_RESET:
		begin
         next_state = STATE_IDLE;
		end
		STATE_IDLE:
		begin
			if(~usbCtrlInFIFO_empty & ~inUsbFIFO_full)begin
            usbCtrlInFIFO_rdreq = 1;
            next_state = STATE_VERIFY_SYSTEM_PROCESSING_ACTIVATION;
         end
      end
      STATE_VERIFY_SYSTEM_PROCESSING_ACTIVATION:
      begin
         if(usbCtrlInFIFO_dout[7:1] == 7'b1111111)       // turn on/off neuron processing
            next_state = STATE_IDLE;
         else
            next_state = STATE_RX_USB_DATA;              // normal data (dendrite, configuration, etc.)
      end
      STATE_RX_USB_DATA:
      begin
         inUsbFIFO_wrreq = 1;
			next_state = STATE_IDLE;
      end
      default:
      begin
         usbCtrlInFIFO_rdreq = 0;
			inUsbFIFO_wrreq     = 0;
         next_state = STATE_RESET;
      end
   endcase
end


/////////////////////////////////////
// USB TRANSMIT DATA STATE MACHINE //
/////////////////////////////////////

reg [1:0] curr_state_tx;
reg [1:0] next_state_tx;

parameter [1:0] STATE_TX_RESET    = 2'd0;
parameter [1:0] STATE_TX_IDLE     = 2'd1;
parameter [1:0] STATE_TX_USB_DATA = 2'd2;

always @(posedge rst or posedge clk)
begin
	if(rst)
		curr_state_tx <= STATE_TX_RESET;
   else
		curr_state_tx <= next_state_tx;
end

always @(*)
begin
   outUsbFIFO_rdreq     = 0;
   usbCtrlOutFIFO_wrreq = 0;
   
   next_state_tx = curr_state_tx;

	case(curr_state_tx)
		STATE_TX_RESET:
		begin
         next_state_tx = STATE_TX_IDLE;
		end
		STATE_TX_IDLE:
		begin
			if(~outUsbFIFO_empty & ~usbCtrlOutFIFO_full)begin
            outUsbFIFO_rdreq = 1;
            next_state_tx = STATE_TX_USB_DATA;
         end
      end
      STATE_TX_USB_DATA:
      begin
         usbCtrlOutFIFO_wrreq = 1;
			next_state_tx = STATE_TX_IDLE;
      end
      default:
      begin
         outUsbFIFO_rdreq     = 0;
			usbCtrlOutFIFO_wrreq = 0;
         next_state_tx = STATE_TX_RESET;
      end
   endcase
end


endmodule
