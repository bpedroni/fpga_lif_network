
module usb_arbiter_interface_multi_fifos
   #(parameter 
      USB_WIDTH_BITS=8,
      FIFO_DISTANT_WIDTH_BITS=24) 
   (
      input rst,
      input clk,

      //output reg processCell_en,
      output reg processCell_en,
      
      input processCell,
      input [9:0] global_time,
      
      
      ///////////////////////
      // INCOMING USB PATH //
      ///////////////////////
      
		input											 usbCtrlInFIFO_empty,		// from usb_fifo_control to interface
      output reg                       	 usbCtrlInFIFO_rdreq,
		input [USB_WIDTH_BITS-1:0]       	 usbCtrlInFIFO_dout,

      output                           	 inUsbFIFO_empty,				// from interface to arbiter_top
      input                            	 inUsbFIFO_rdreq,
      output [FIFO_DISTANT_WIDTH_BITS-1:0] inUsbFIFO_dout,
      
      
      ///////////////////////
      // OUTGOING USB PATH //
      ///////////////////////
      
      output                          		outUsbFIFO_full,				// from arbiter_top to interface
      input [FIFO_DISTANT_WIDTH_BITS-1:0] outUsbFIFO_din,
      input                           	   outUsbFIFO_wrreq,

      input                           	   usbCtrlOutFIFO_full,			// from interface to usb_fifo_control
		output [USB_WIDTH_BITS-1:0]     	   usbCtrlOutFIFO_din,			
      output reg                      	   usbCtrlOutFIFO_wrreq
      
   );

reg globalTime_en;

always @(posedge rst or posedge clk)
begin
   if(rst)begin
      globalTime_en  <= 1'b0;
      processCell_en <= 1'b0;
   end else if(usbCtrlInFIFO_dout[7:2] == 6'b111111)begin
      globalTime_en  <= usbCtrlInFIFO_dout[1];
      processCell_en <= usbCtrlInFIFO_dout[0];
   end
end

   
////////////////////
// Input USB FIFO //
////////////////////
reg       move_to_next_incoming_byte;
reg [1:0] current_incoming_byte;

reg [2:0] inUsbFIFO_wrreq;
wire [2:0] inUsbFIFO_full, inUsbFIFO_empty_grouped;
wire [USB_WIDTH_BITS-1:0] inUsbFIFO_dout_grouped [2:0];

assign inUsbFIFO_empty = inUsbFIFO_empty_grouped[2] | inUsbFIFO_empty_grouped[1] | inUsbFIFO_empty_grouped[0];
assign inUsbFIFO_dout = {inUsbFIFO_dout_grouped[2], inUsbFIFO_dout_grouped[1], inUsbFIFO_dout_grouped[0]};

fifo_8x8 USB_FIFO_IN_2
   (clk, rst, usbCtrlInFIFO_dout, inUsbFIFO_wrreq[2], inUsbFIFO_rdreq, 
    inUsbFIFO_dout_grouped[2], inUsbFIFO_full[2], inUsbFIFO_empty_grouped[2]);
fifo_8x8 USB_FIFO_IN_1
   (clk, rst, usbCtrlInFIFO_dout, inUsbFIFO_wrreq[1], inUsbFIFO_rdreq, 
    inUsbFIFO_dout_grouped[1], inUsbFIFO_full[1], inUsbFIFO_empty_grouped[1]);
fifo_8x8 USB_FIFO_IN_0
   (clk, rst, usbCtrlInFIFO_dout, inUsbFIFO_wrreq[0], inUsbFIFO_rdreq, 
    inUsbFIFO_dout_grouped[0], inUsbFIFO_full[0], inUsbFIFO_empty_grouped[0]);
    

/////////////////////
// Output USB FIFO //
/////////////////////
reg [1:0] current_outgoing_byte;

reg [2:0] outUsbFIFO_rdreq;
wire outUsbFIFO_empty;        // just one FIFO is necessary (same for FULL signal)

wire [USB_WIDTH_BITS-1:0] outUsbFIFO_dout_grouped [2:0];

wire [USB_WIDTH_BITS-1:0] global_time_out [1:0];
assign global_time_out[1] = {6'b111111, global_time[9:8]};
assign global_time_out[0] = global_time[7:0];
reg send_global_time, global_time_select;

assign usbCtrlOutFIFO_din = send_global_time ? global_time_out[global_time_select] : outUsbFIFO_dout_grouped[current_outgoing_byte];

fifo_8x8 USB_FIFO_OUT2
   (clk, rst, outUsbFIFO_din[23:16], outUsbFIFO_wrreq, outUsbFIFO_rdreq[2],
    outUsbFIFO_dout_grouped[2], outUsbFIFO_full, outUsbFIFO_empty);
fifo_8x8 USB_FIFO_OUT1
   (clk, rst, outUsbFIFO_din[15:08], outUsbFIFO_wrreq, outUsbFIFO_rdreq[1],
    outUsbFIFO_dout_grouped[1], , );
fifo_8x8 USB_FIFO_OUT0
   (clk, rst, outUsbFIFO_din[07:00], outUsbFIFO_wrreq, outUsbFIFO_rdreq[0],
    outUsbFIFO_dout_grouped[0], , );


////////////////////////////////////
// USB RECEIVE DATA STATE MACHINE //
////////////////////////////////////

reg [1:0] curr_state;
reg [1:0] next_state;

parameter [1:0] STATE_RESET                               = 2'd0;
parameter [1:0] STATE_IDLE                                = 2'd1;
parameter [1:0] STATE_VERIFY_SYSTEM_PROCESSING_ACTIVATION = 2'd2;
parameter [1:0] STATE_RX_USB_DATA                         = 2'd3;

always @(posedge rst or posedge clk)
begin
	if(rst)
		curr_state <= STATE_RESET;
   else
		curr_state <= next_state;
end

always @(*)
begin
   usbCtrlInFIFO_rdreq        = 0;
   inUsbFIFO_wrreq            = 3'b000;
   move_to_next_incoming_byte = 0;
   
   next_state = curr_state;

	case(curr_state)
		STATE_RESET:
		begin
         next_state = STATE_IDLE;
		end
		STATE_IDLE:
		begin
			if(~usbCtrlInFIFO_empty & ~inUsbFIFO_full[current_incoming_byte])begin
            usbCtrlInFIFO_rdreq = 1;
            next_state = STATE_VERIFY_SYSTEM_PROCESSING_ACTIVATION;
         end
      end
      STATE_VERIFY_SYSTEM_PROCESSING_ACTIVATION:
      begin
         if(usbCtrlInFIFO_dout[7:2] == 6'b111111)        // turn on/off neuron processing
            next_state = STATE_IDLE;
         else
            next_state = STATE_RX_USB_DATA;              // normal data (dendrite, configuration, etc.)
      end
      STATE_RX_USB_DATA:
      begin
         inUsbFIFO_wrreq[current_incoming_byte] = 1;
         move_to_next_incoming_byte = 1;
			next_state = STATE_IDLE;
      end
      default:
      begin
         usbCtrlInFIFO_rdreq        = 0;
			inUsbFIFO_wrreq            = 3'b000;
         move_to_next_incoming_byte = 0;
         next_state = STATE_RESET;
      end
   endcase
end

always @(posedge rst or posedge clk)
begin
   if(rst)
      current_incoming_byte <= 2;
   else if(move_to_next_incoming_byte)begin
      if(current_incoming_byte > 0)
         current_incoming_byte <= current_incoming_byte - 1;
      else
         current_incoming_byte <= 2;
   end
end


/////////////////////////////////////
// USB TRANSMIT DATA STATE MACHINE //
/////////////////////////////////////

reg [3:0] curr_state_tx;
reg [3:0] next_state_tx;

parameter [3:0] STATE_TX_RESET          = 4'd0;
parameter [3:0] STATE_TX_IDLE           = 4'd1;
parameter [3:0] STATE_TX_USB_DATA2      = 4'd2;
parameter [3:0] STATE_TX_READ_USB_DATA1 = 4'd3;
parameter [3:0] STATE_TX_USB_DATA1      = 4'd4;
parameter [3:0] STATE_TX_READ_USB_DATA0 = 4'd5;
parameter [3:0] STATE_TX_USB_DATA0      = 4'd6;
parameter [3:0] STATE_SEND_GLOBAL_TIME1 = 4'd7;
parameter [3:0] STATE_SEND_GLOBAL_TIME2 = 4'd8;


always @(posedge rst or posedge clk)
begin
	if(rst)
		curr_state_tx <= STATE_TX_RESET;
   else
		curr_state_tx <= next_state_tx;
end

always @(*)
begin
   outUsbFIFO_rdreq      = 3'b000;
   usbCtrlOutFIFO_wrreq  = 0;
   current_outgoing_byte = 2;
   send_global_time      = 0;
   global_time_select    = 1;

   next_state_tx = curr_state_tx;
   
	case(curr_state_tx)
		STATE_TX_RESET:
		begin
         next_state_tx = STATE_TX_IDLE;
		end
		STATE_TX_IDLE:
		begin
			if(~outUsbFIFO_empty)begin
            outUsbFIFO_rdreq[2] = 1;
            current_outgoing_byte = 2;
            next_state_tx = STATE_TX_USB_DATA2;
         end else if (processCell & globalTime_en)
            next_state_tx = STATE_SEND_GLOBAL_TIME1;
      end
      STATE_TX_USB_DATA2:
      begin
         current_outgoing_byte = 2;
         if(~usbCtrlOutFIFO_full)begin
            usbCtrlOutFIFO_wrreq = 1;
            next_state_tx = STATE_TX_READ_USB_DATA1;
         end
      end
      STATE_TX_READ_USB_DATA1:
      begin
         outUsbFIFO_rdreq[1] = 1;
         current_outgoing_byte = 1;
         next_state_tx = STATE_TX_USB_DATA1;
      end
      STATE_TX_USB_DATA1:
      begin
         current_outgoing_byte = 1;
         if(~usbCtrlOutFIFO_full)begin
            usbCtrlOutFIFO_wrreq = 1;
            next_state_tx = STATE_TX_READ_USB_DATA0;
         end
      end
      STATE_TX_READ_USB_DATA0:
      begin
         outUsbFIFO_rdreq[0] = 1;
         current_outgoing_byte = 0;
         next_state_tx = STATE_TX_USB_DATA0;
      end
      STATE_TX_USB_DATA0:
      begin
         current_outgoing_byte = 0;
         if(~usbCtrlOutFIFO_full)begin
            usbCtrlOutFIFO_wrreq = 1;
            next_state_tx = STATE_TX_IDLE;
         end
      end
      
      STATE_SEND_GLOBAL_TIME1:
      begin
         send_global_time   = 1;
         global_time_select = 1;
         if(~usbCtrlOutFIFO_full)begin
            usbCtrlOutFIFO_wrreq = 1;
            next_state_tx = STATE_SEND_GLOBAL_TIME2;
         end
      end
      STATE_SEND_GLOBAL_TIME2:
      begin
         send_global_time   = 1;
         global_time_select = 0;
         if(~usbCtrlOutFIFO_full)begin
            usbCtrlOutFIFO_wrreq = 1;
            next_state_tx = STATE_TX_IDLE;
         end
      end
      
      default:
      begin
         outUsbFIFO_rdreq      = 3'b000;
			usbCtrlOutFIFO_wrreq  = 0;
         current_outgoing_byte = 2;
         send_global_time      = 0;
         global_time_select    = 1;
         next_state_tx = STATE_TX_RESET;
      end
   endcase
end


endmodule
