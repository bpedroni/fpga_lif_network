
module arbiter 
   #(parameter 
      ADDRESS_FILTER_START=15,
      ADDRESS_FILTER_STOP=13,
      ADDRESS_FILTER=0,
      GLOBAL_CELL_BITS=8,
      FIFO_WIDTH_BITS=24)
   (
      input rst,
      input clk,
      

      /////////////////////
      // INCOMING EVENTS //
      /////////////////////
      
      output                      inDownFIFO_full,
      input [FIFO_WIDTH_BITS-1:0] inDownFIFO_din,
      input                       inDownFIFO_wrreq,
      
      input                       inUpLeftFIFO_empty,
      output reg                  inUpLeftFIFO_rdreq,
      input [FIFO_WIDTH_BITS-1:0] inUpLeftFIFO_dout,
      
      input                       inUpRightFIFO_empty,
      output reg                  inUpRightFIFO_rdreq,
      input [FIFO_WIDTH_BITS-1:0] inUpRightFIFO_dout,
      
      
      /////////////////////
      // OUTGOING EVENTS //
      /////////////////////
      
      output                       outUpFIFO_empty,
      input                        outUpFIFO_rdreq,
      output [FIFO_WIDTH_BITS-1:0] outUpFIFO_dout,
      
      input                        outDownLeftFIFO_full,
      output [FIFO_WIDTH_BITS-1:0] outDownLeftFIFO_din,
      output reg                   outDownLeftFIFO_wrreq,
      
      input                        outDownRightFIFO_full,
      output [FIFO_WIDTH_BITS-1:0] outDownRightFIFO_din,
      output reg                   outDownRightFIFO_wrreq
      );


reg move_to_next_path;
reg [1:0] incoming_path_being_serviced;

////////////////////      
// Incoming UP FIFO
wire inDownFIFO_empty;
reg inDownFIFO_rdreq;
wire [FIFO_WIDTH_BITS-1:0] inDownFIFO_dout;
      
fifo_24x24 FIFO_IN_UP 
	(clk, rst, inDownFIFO_din, inDownFIFO_wrreq, inDownFIFO_rdreq,
      inDownFIFO_dout, inDownFIFO_full, inDownFIFO_empty);

////////////////////
// Outgoing UP FIFO
wire outUpFIFO_full;
wire [FIFO_WIDTH_BITS-1:0] outUpFIFO_din;
reg outUpFIFO_wrreq;

fifo_24x24 FIFO_OUT_UP 
	(clk, rst, outUpFIFO_din, outUpFIFO_wrreq, outUpFIFO_rdreq,
	 outUpFIFO_dout, outUpFIFO_full, outUpFIFO_empty);

/////////////////////////////////
// Incoming path arbitration
always @(posedge rst or posedge clk)
begin
   if(rst)
      incoming_path_being_serviced <= 0;
   else if(move_to_next_path)begin
      if(incoming_path_being_serviced < 2)
         incoming_path_being_serviced <= incoming_path_being_serviced + 1;
      else
         incoming_path_being_serviced <= 0;
   end
end

// incoming empty
wire [2:0] mux_empty = {inUpRightFIFO_empty, inUpLeftFIFO_empty, inDownFIFO_empty};
wire incomingPath_empty = mux_empty[incoming_path_being_serviced];

// incoming rdreq and dout
reg incomingPath_rdreq;
reg [FIFO_WIDTH_BITS-1:0] incomingPath_dout;

always @(*)begin
   inDownFIFO_rdreq    = 0;
   inUpLeftFIFO_rdreq  = 0;
   inUpRightFIFO_rdreq = 0;
   
   incomingPath_dout = 0;
   
   case(incoming_path_being_serviced)
      0: begin
         inDownFIFO_rdreq  = incomingPath_rdreq;
         incomingPath_dout = inDownFIFO_dout;
      end
      1: begin
         inUpLeftFIFO_rdreq = incomingPath_rdreq;
         incomingPath_dout  = inUpLeftFIFO_dout;
      end
      2: begin
         inUpRightFIFO_rdreq = incomingPath_rdreq;
         incomingPath_dout   = inUpRightFIFO_dout;
      end 
      default: begin
         inDownFIFO_rdreq    = 0;
         inUpLeftFIFO_rdreq  = 0;
         inUpRightFIFO_rdreq = 0;
         incomingPath_dout   = 0;
      end
   endcase
end


/////////////////////////////////////////////////////////
// Output data is written based on state machine (wrreq)
assign outUpFIFO_din        = incomingPath_dout;
assign outDownLeftFIFO_din  = incomingPath_dout;
assign outDownRightFIFO_din = incomingPath_dout;


/////////////////////////////////////
// state machine: servicing event
parameter [3:0] STATE_RESET                 = 4'd00;
parameter [3:0] STATE_IDLE                  = 4'd01;
parameter [3:0] STATE_VERIFY_DIRECTION      = 4'd02;
parameter [3:0] STATE_VERIFY_DOWN           = 4'd03;
parameter [3:0] STATE_SEND_LEFT1            = 4'd04;
parameter [3:0] STATE_READ_LEFT2            = 4'd05;
parameter [3:0] STATE_SEND_LEFT2            = 4'd06;
parameter [3:0] STATE_SEND_RIGHT1           = 4'd07;
parameter [3:0] STATE_READ_RIGHT2           = 4'd08;
parameter [3:0] STATE_SEND_RIGHT2           = 4'd09;
parameter [3:0] STATE_VERIFY_ADDRESS_FILTER = 4'd10;
parameter [3:0] STATE_SEND_UP1              = 4'd11;
parameter [3:0] STATE_READ_UP2              = 4'd12;
parameter [3:0] STATE_SEND_UP2              = 4'd13;

reg [3:0] curr_state;
reg [3:0] next_state;

always @(posedge rst or posedge clk)
begin
   if(rst)
      curr_state <= STATE_RESET;
   else
      curr_state <= next_state;
end

always @(*)
begin

   move_to_next_path      = 0;
   incomingPath_rdreq     = 0;
   outUpFIFO_wrreq        = 0;
   outDownLeftFIFO_wrreq  = 0;
   outDownRightFIFO_wrreq = 0;
      
   next_state = curr_state;

   case(curr_state)
      STATE_RESET:
      begin
         next_state = STATE_IDLE;
      end
      
      STATE_IDLE:
      begin
         if(~incomingPath_empty)begin
            incomingPath_rdreq = 1;
            next_state = STATE_VERIFY_DIRECTION;
         end else
            move_to_next_path = 1;
      end
      
      STATE_VERIFY_DIRECTION:
      begin
         if(incoming_path_being_serviced==0)                   // configuration or dendrite event
            next_state = STATE_VERIFY_DOWN;
         else if(~incomingPath_dout[FIFO_WIDTH_BITS-1])        // dendrite event
            next_state = STATE_VERIFY_ADDRESS_FILTER;          
         else begin                                            // probe event
            if(incomingPath_dout[FIFO_WIDTH_BITS-2])             // Vmem probe event
               next_state = STATE_SEND_UP1;
            else                                                 // spike probe event
               next_state = STATE_SEND_UP2;
         end
      end

      STATE_VERIFY_DOWN:
      begin
         if(~incomingPath_dout[ADDRESS_FILTER_STOP-1])begin    // left
            if(incomingPath_dout[FIFO_WIDTH_BITS-1])              // configuration event
               next_state = STATE_SEND_LEFT1;
            else                                                  // dendrite event
               next_state = STATE_SEND_LEFT2;
         
         end else begin                                        // right
            if(incomingPath_dout[FIFO_WIDTH_BITS-1])              // configuration event
               next_state = STATE_SEND_RIGHT1;
            else
               next_state = STATE_SEND_RIGHT2;                    // dendrite event
         end
      end
      
      // down left: configuration event (LEFT1+LEFT2) or dendrite event (LEFT2)
      STATE_SEND_LEFT1:
      begin
         if(~outDownLeftFIFO_full)begin
            outDownLeftFIFO_wrreq = 1;
            next_state = STATE_READ_LEFT2;
         end
      end
      STATE_READ_LEFT2:
      begin
         if(~incomingPath_empty)begin
            incomingPath_rdreq = 1;
            next_state = STATE_SEND_LEFT2;
         end
      end
      STATE_SEND_LEFT2:
      begin
         if(~outDownLeftFIFO_full)begin
            outDownLeftFIFO_wrreq = 1;
            move_to_next_path = 1;
            next_state = STATE_IDLE;
         end
      end
      
      // down right: configuration event (RIGHT1+RIGHT2) or dendrite event (RIGHT2)
      STATE_SEND_RIGHT1:
      begin
         if(~outDownRightFIFO_full)begin
            outDownRightFIFO_wrreq = 1;
            next_state = STATE_READ_RIGHT2;
         end
      end
      STATE_READ_RIGHT2:
      begin
         if(~incomingPath_empty)begin
            incomingPath_rdreq = 1;
            next_state = STATE_SEND_RIGHT2;
         end
      end
      STATE_SEND_RIGHT2:
      begin
         if(~outDownRightFIFO_full)begin
            outDownRightFIFO_wrreq = 1;
            move_to_next_path = 1;
            next_state = STATE_IDLE;
         end
      end
      
      // dendrite event: verify if should go down (same ADDRESS_FILTER) or up (diff. ADDRESS_FILTER)
      STATE_VERIFY_ADDRESS_FILTER:
      begin
         if(incomingPath_dout[ADDRESS_FILTER_START:ADDRESS_FILTER_STOP] == ADDRESS_FILTER)begin      // if is in the same layer, route down
            if(~incomingPath_dout[ADDRESS_FILTER_STOP-1])                                            // verify if left(0) or right (1)
               next_state = STATE_SEND_LEFT2;
            else
               next_state = STATE_SEND_RIGHT2;
         end else                                                                                    // route to upper layer
            next_state = STATE_SEND_UP2;
      end
      
      // up Vmem probe event (UP1+UP2), spike probe event (UP2), or dendrite event (UP2)
      STATE_SEND_UP1:
      begin
         if(~outUpFIFO_full)begin
            outUpFIFO_wrreq = 1;
            next_state = STATE_READ_UP2;
         end
      end
      STATE_READ_UP2:
      begin
         if(~incomingPath_empty)begin
            incomingPath_rdreq = 1;
            next_state = STATE_SEND_UP2;
         end
      end
      STATE_SEND_UP2:
      begin
         if(~outUpFIFO_full)begin
            outUpFIFO_wrreq = 1;
            move_to_next_path = 1;
            next_state = STATE_IDLE;
         end
      end
 
      ///////////
      default:
      begin
         move_to_next_path      = 0;
         incomingPath_rdreq     = 0;
         outUpFIFO_wrreq        = 0;
         outDownLeftFIFO_wrreq  = 0;
         outDownRightFIFO_wrreq = 0;
         next_state = STATE_RESET;
      end
   endcase
end

endmodule

      