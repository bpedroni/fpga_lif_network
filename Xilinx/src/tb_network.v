
module tb_network
   #(parameter 
      USB_WIDTH_BITS=8,
      FIFO_WIDTH_BITS=24,
      GLOBAL_CELL_BITS=8,
      CELLS=16,
      CELL_BITS=4,
      SYNAPSE_BITS=5,
      DENDRITE_BITS=5,
      VMEM_BITS=12)
   (

		input RESET,
		input SYSCLK_P,
		input SYSCLK_N,
		
		// input and output connected to USB FIFO (FT245R)
		input 	   FT_RXF,
		output 	   FT_RD,
		inout [7:0] FT_D,
		input 		FT_TXE,
		output 		FT_WR,
      
      output FPGA_OUT_PROBE0

   );


wire rst = RESET;
wire synapseType = 1'b0;
wire processCell_en;
reg [9:0] global_time;
		
//////////////////////////////
// CLOCK BUFFER AND DIVIDERS

// Differential buffer for clock
IBUFGDS #(.DIFF_TERM("TRUE")) sysbuf(.O(CLK_200M), .I(SYSCLK_P), .IB(SYSCLK_N));

// Clock dividers
PLL CLOCK_RESOURCE (
.CLK_IN1(CLK_200M),
.CLK_OUT1(CLK_125M),
.CLK_OUT2(CLK_20M),
.CLK_OUT3(CLK_10M),
.CLK_OUT4(CLK_5M),
.RESET(rst));

wire clk = CLK_20M;
wire clk_usb = CLK_5M;

// Global 'tick': processCell
reg [21:0] clk_div;
always @(posedge rst or posedge CLK_20M)
begin
	if(rst)
		clk_div <= 21'd1;
	else if(processCell_en)
		clk_div <= clk_div + 21'd1;
   else
		clk_div <= 21'd1;
end
wire processCell = (clk_div == 21'd0);

always @(posedge rst or posedge CLK_20M)
begin
	if(rst)
		global_time <= 0;
	else if(processCell)
		global_time <= global_time + 1;
end


///////////////////
// NEURON CORES
wire [15:0] dendDistantDownFIFO_full, dendDistantDownFIFO_wrreq;
wire [FIFO_WIDTH_BITS-1:0] dendDistantDownFIFO_din [15:0];

wire [15:0] dendDistantUpFIFO_empty, dendDistantUpFIFO_rdreq;
wire [FIFO_WIDTH_BITS-1:0] dendDistantUpFIFO_dout [15:0];

assign FPGA_OUT_PROBE0 = dendDistantDownFIFO_wrreq[0];

genvar i;
generate 
   for(i=0; i<16; i=i+1)begin : gen_nc
      neuron_core 
         #(i,
            GLOBAL_CELL_BITS, CELLS, CELL_BITS, SYNAPSE_BITS, DENDRITE_BITS, FIFO_WIDTH_BITS, VMEM_BITS)
      NEURON_CORE_i
         (rst, clk, processCell, synapseType,
          dendDistantDownFIFO_full[i], dendDistantDownFIFO_din[i], dendDistantDownFIFO_wrreq[i],
          dendDistantUpFIFO_empty[i], dendDistantUpFIFO_rdreq[i], dendDistantUpFIFO_dout[i]);
   end
endgenerate
      
//////////////////////
// ARBITERS: LAYER 1
wire [7:0] L1_inDown_full, L1_inDown_wrreq;
wire [FIFO_WIDTH_BITS-1:0] L1_inDown_din [7:0];
wire [7:0] L1_outUp_empty, L1_outUp_rdreq;
wire [FIFO_WIDTH_BITS-1:0] L1_outUp_dout [7:0];

genvar j;
generate
   for(j=0; j<8; j=j+1)begin : gen_l1
      arbiter #(GLOBAL_CELL_BITS+7, GLOBAL_CELL_BITS+5, j, 
                GLOBAL_CELL_BITS, FIFO_WIDTH_BITS)
      ARBITER_L1_j 
         (rst, clk, 
         L1_inDown_full[j],               L1_inDown_din[j],               L1_inDown_wrreq[j],
         dendDistantUpFIFO_empty[2*j],    dendDistantUpFIFO_rdreq[2*j],   dendDistantUpFIFO_dout[2*j],
         dendDistantUpFIFO_empty[2*j+1],  dendDistantUpFIFO_rdreq[2*j+1], dendDistantUpFIFO_dout[2*j+1],
         L1_outUp_empty[j],               L1_outUp_rdreq[j],              L1_outUp_dout[j],
         dendDistantDownFIFO_full[2*j],   dendDistantDownFIFO_din[2*j],   dendDistantDownFIFO_wrreq[2*j],
         dendDistantDownFIFO_full[2*j+1], dendDistantDownFIFO_din[2*j+1], dendDistantDownFIFO_wrreq[2*j+1]);
   end
endgenerate

//////////////////////
// ARBITERS: LAYER 2
wire [3:0] L2_inDown_full, L2_inDown_wrreq;
wire [FIFO_WIDTH_BITS-1:0] L2_inDown_din [3:0];
wire [3:0] L2_outUp_empty, L2_outUp_rdreq;
wire [FIFO_WIDTH_BITS-1:0] L2_outUp_dout [3:0];

genvar k;
generate
   for(k=0; k<4; k=k+1)begin : gen_l2
      arbiter #(GLOBAL_CELL_BITS+7, GLOBAL_CELL_BITS+6, k, 
                GLOBAL_CELL_BITS, FIFO_WIDTH_BITS)
      ARBITER_L2_k 
         (rst, clk, 
         L2_inDown_full[k],     L2_inDown_din[k],      L2_inDown_wrreq[k],
         L1_outUp_empty[2*k],   L1_outUp_rdreq[2*k],   L1_outUp_dout[2*k],
         L1_outUp_empty[2*k+1], L1_outUp_rdreq[2*k+1], L1_outUp_dout[2*k+1],
         L2_outUp_empty[k],     L2_outUp_rdreq[k],     L2_outUp_dout[k],
         L1_inDown_full[2*k],   L1_inDown_din[2*k],    L1_inDown_wrreq[2*k],
         L1_inDown_full[2*k+1], L1_inDown_din[2*k+1],  L1_inDown_wrreq[2*k+1]);
   end
endgenerate

//////////////////////
// ARBITERS: LAYER 3
wire [1:0] L3_inDown_full, L3_inDown_wrreq;
wire [FIFO_WIDTH_BITS-1:0] L3_inDown_din [1:0];
wire [1:0] L3_outUp_empty, L3_outUp_rdreq;
wire [FIFO_WIDTH_BITS-1:0] L3_outUp_dout [1:0];

genvar m;
generate
   for(m=0; m<2; m=m+1)begin : gen_l3
      arbiter #(GLOBAL_CELL_BITS+7, GLOBAL_CELL_BITS+7, m, 
                GLOBAL_CELL_BITS, FIFO_WIDTH_BITS)
      ARBITER_L3_m 
         (rst, clk, 
         L3_inDown_full[m],     L3_inDown_din[m],      L3_inDown_wrreq[m],
         L2_outUp_empty[2*m],   L2_outUp_rdreq[2*m],   L2_outUp_dout[2*m],
         L2_outUp_empty[2*m+1], L2_outUp_rdreq[2*m+1], L2_outUp_dout[2*m+1],
         L3_outUp_empty[m],     L3_outUp_rdreq[m],     L3_outUp_dout[m],
         L2_inDown_full[2*m],   L2_inDown_din[2*m],    L2_inDown_wrreq[2*m],
         L2_inDown_full[2*m+1], L2_inDown_din[2*m+1],  L2_inDown_wrreq[2*m+1]);
   end
endgenerate

//////////////////////
// ARBITERS: LAYER 4
wire L4_inDown_full, L4_inDown_wrreq;
wire [FIFO_WIDTH_BITS-1:0] L4_inDown_din;
wire L4_outUp_empty, L4_outUp_rdreq;
wire [FIFO_WIDTH_BITS-1:0] L4_outUp_dout;

wire inUsbFIFO_empty, inUsbFIFO_rdreq;
wire [FIFO_WIDTH_BITS-1:0] inUsbFIFO_dout;

wire outUsbFIFO_full, outUsbFIFO_wrreq;
wire [FIFO_WIDTH_BITS-1:0] outUsbFIFO_din;

arbiter_top #(GLOBAL_CELL_BITS, FIFO_WIDTH_BITS)
      ARBITER_L4
         (rst, clk, global_time,
          inUsbFIFO_empty, inUsbFIFO_rdreq, inUsbFIFO_dout,
          L3_outUp_empty[0], L3_outUp_rdreq[0], L3_outUp_dout[0],
          L3_outUp_empty[1], L3_outUp_rdreq[1], L3_outUp_dout[1],
          outUsbFIFO_full, outUsbFIFO_din, outUsbFIFO_wrreq,
          L3_inDown_full[0], L3_inDown_din[0],  L3_inDown_wrreq[0],
          L3_inDown_full[1], L3_inDown_din[1],  L3_inDown_wrreq[1]);

///////////////////
// USB CONTROLLER 
wire usbCtrlInFIFO_empty, usbCtrlInFIFO_rdreq, usbCtrlOutFIFO_full, usbCtrlOutFIFO_wrreq;
wire [USB_WIDTH_BITS-1:0] usbCtrlInFIFO_dout, usbCtrlOutFIFO_din;

usb_arbiter_interface_multi_fifos #(USB_WIDTH_BITS, FIFO_WIDTH_BITS)
	USB_ARBITER_INTERFACE (rst, clk, processCell_en, processCell, global_time,
								  usbCtrlInFIFO_empty, usbCtrlInFIFO_rdreq, usbCtrlInFIFO_dout,
								  inUsbFIFO_empty, inUsbFIFO_rdreq, inUsbFIFO_dout,
								  outUsbFIFO_full, outUsbFIFO_din, outUsbFIFO_wrreq,
								  usbCtrlOutFIFO_full, usbCtrlOutFIFO_din, usbCtrlOutFIFO_wrreq);

wire   FT_RD2, FT_WR2;
assign FT_RD = FT_RD2;
assign FT_WR = FT_WR2;

usb_fifo_control USB_CTRL
	(clk_usb, clk, rst,
	 FT_RXF, FT_RD2, FT_D, FT_TXE, FT_WR2,
	 usbCtrlInFIFO_empty, usbCtrlInFIFO_rdreq, usbCtrlInFIFO_dout,
    usbCtrlOutFIFO_full, usbCtrlOutFIFO_din, usbCtrlOutFIFO_wrreq);

endmodule
