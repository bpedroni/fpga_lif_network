library verilog;
use verilog.vl_types.all;
entity router_outgoing is
    generic(
        ADDRESS_FILTER  : vl_logic_vector(0 to 3) := (Hi0, Hi0, Hi0, Hi0);
        GLOBAL_CELL_BITS: integer := 10;
        CELL_BITS       : integer := 6;
        SYNAPSE_BITS    : integer := 6;
        DENDRITE_BITS   : integer := 6;
        VMEM_BITS       : integer := 12;
        FIFO_WIDTH_BITS : integer := 24
    );
    port(
        rst             : in     vl_logic;
        clk             : in     vl_logic;
        inSpikeEventFIFO_empty: in     vl_logic;
        inSpikeEventFIFO_rdreq: out    vl_logic;
        inSpikeEventFIFO_dout: in     vl_logic_vector;
        configRoutingTableFIFO_empty: in     vl_logic;
        configRoutingTableFIFO_rdreq: out    vl_logic;
        configRoutingTableFIFO_dout: in     vl_logic_vector;
        outLocalFIFO_full: in     vl_logic;
        outLocalFIFO_din: out    vl_logic_vector;
        outLocalFIFO_wrreq: out    vl_logic;
        outDistantFIFO_full: in     vl_logic;
        outDistantFIFO_din: out    vl_logic_vector;
        outDistantFIFO_wrreq: out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of ADDRESS_FILTER : constant is 1;
    attribute mti_svvh_generic_type of GLOBAL_CELL_BITS : constant is 1;
    attribute mti_svvh_generic_type of CELL_BITS : constant is 1;
    attribute mti_svvh_generic_type of SYNAPSE_BITS : constant is 1;
    attribute mti_svvh_generic_type of DENDRITE_BITS : constant is 1;
    attribute mti_svvh_generic_type of VMEM_BITS : constant is 1;
    attribute mti_svvh_generic_type of FIFO_WIDTH_BITS : constant is 1;
end router_outgoing;
