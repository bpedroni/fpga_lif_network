library verilog;
use verilog.vl_types.all;
entity controller_usb is
    generic(
        USB_DEPTH_BITS  : integer := 5;
        USB_WIDTH_BITS  : integer := 8;
        FIFO_USB_WIDTH_BITS: integer := 16
    );
    port(
        rst             : in     vl_logic;
        clk             : in     vl_logic;
        usb_data        : in     vl_logic_vector;
        usb_req         : in     vl_logic;
        usb_ack         : out    vl_logic;
        inUsbFIFO_empty : out    vl_logic;
        inUsbFIFO_rdreq : in     vl_logic;
        inUsbFIFO_dout  : out    vl_logic_vector;
        outUsbFIFO_full : out    vl_logic;
        outUsbFIFO_din  : in     vl_logic_vector;
        outUsbFIFO_wrreq: in     vl_logic;
        usbout_data     : out    vl_logic_vector;
        usbout_req      : out    vl_logic;
        usbout_ack      : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of USB_DEPTH_BITS : constant is 1;
    attribute mti_svvh_generic_type of USB_WIDTH_BITS : constant is 1;
    attribute mti_svvh_generic_type of FIFO_USB_WIDTH_BITS : constant is 1;
end controller_usb;
