library verilog;
use verilog.vl_types.all;
entity router_incoming is
    generic(
        CELL_BITS       : integer := 4;
        DENDRITE_BITS   : integer := 4;
        FIFO_WIDTH_BITS : integer := 24
    );
    port(
        rst             : in     vl_logic;
        clk             : in     vl_logic;
        inDistantFIFO_empty: in     vl_logic;
        inDistantFIFO_rdreq: out    vl_logic;
        inDistantFIFO_dout: in     vl_logic_vector;
        inLocalFIFO_empty: in     vl_logic;
        inLocalFIFO_rdreq: out    vl_logic;
        inLocalFIFO_dout: in     vl_logic_vector;
        outDendriteEventFIFO_full: in     vl_logic;
        outDendriteEventFIFO_din: out    vl_logic_vector;
        outDendriteEventFIFO_wrreq: out    vl_logic;
        configRoutingTableFIFO_full: in     vl_logic;
        configRoutingTableFIFO_din: out    vl_logic_vector;
        configRoutingTableFIFO_wrreq: out    vl_logic;
        configWeightFIFO_full: in     vl_logic;
        configWeightFIFO_din: out    vl_logic_vector;
        configWeightFIFO_wrreq: out    vl_logic;
        configCellFIFO_full: in     vl_logic;
        configCellFIFO_din: out    vl_logic_vector;
        configCellFIFO_wrreq: out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of CELL_BITS : constant is 1;
    attribute mti_svvh_generic_type of DENDRITE_BITS : constant is 1;
    attribute mti_svvh_generic_type of FIFO_WIDTH_BITS : constant is 1;
end router_incoming;
