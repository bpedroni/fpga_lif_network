library verilog;
use verilog.vl_types.all;
entity tb_network is
    generic(
        USB_WIDTH_BITS  : integer := 8;
        FIFO_WIDTH_BITS : integer := 24;
        GLOBAL_CELL_BITS: integer := 8;
        CELLS           : integer := 16;
        CELL_BITS       : integer := 4;
        SYNAPSE_BITS    : integer := 5;
        DENDRITE_BITS   : integer := 5;
        VMEM_BITS       : integer := 12
    );
    port(
        RESET           : in     vl_logic;
        SYSCLK_P        : in     vl_logic;
        SYSCLK_N        : in     vl_logic;
        FT_RXF          : in     vl_logic;
        FT_RD           : out    vl_logic;
        FT_D            : inout  vl_logic_vector(7 downto 0);
        FT_TXE          : in     vl_logic;
        FT_WR           : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of USB_WIDTH_BITS : constant is 1;
    attribute mti_svvh_generic_type of FIFO_WIDTH_BITS : constant is 1;
    attribute mti_svvh_generic_type of GLOBAL_CELL_BITS : constant is 1;
    attribute mti_svvh_generic_type of CELLS : constant is 1;
    attribute mti_svvh_generic_type of CELL_BITS : constant is 1;
    attribute mti_svvh_generic_type of SYNAPSE_BITS : constant is 1;
    attribute mti_svvh_generic_type of DENDRITE_BITS : constant is 1;
    attribute mti_svvh_generic_type of VMEM_BITS : constant is 1;
end tb_network;
