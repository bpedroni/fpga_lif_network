library verilog;
use verilog.vl_types.all;
entity arbiter is
    generic(
        ADDRESS_FILTER_START: integer := 12;
        ADDRESS_FILTER_STOP: integer := 9;
        ADDRESS_FILTER  : vl_logic_vector(0 to 3) := (Hi0, Hi0, Hi0, Hi0);
        GLOBAL_CELL_BITS: integer := 8;
        DENDRITE_BITS   : integer := 5;
        FIFO_WIDTH_BITS : integer := 24
    );
    port(
        rst             : in     vl_logic;
        clk             : in     vl_logic;
        inDownFIFO_full : out    vl_logic;
        inDownFIFO_din  : in     vl_logic_vector;
        inDownFIFO_wrreq: in     vl_logic;
        inUpLeftFIFO_empty: in     vl_logic;
        inUpLeftFIFO_rdreq: out    vl_logic;
        inUpLeftFIFO_dout: in     vl_logic_vector;
        inUpRightFIFO_empty: in     vl_logic;
        inUpRightFIFO_rdreq: out    vl_logic;
        inUpRightFIFO_dout: in     vl_logic_vector;
        outUpFIFO_empty : out    vl_logic;
        outUpFIFO_rdreq : in     vl_logic;
        outUpFIFO_dout  : out    vl_logic_vector;
        outDownLeftFIFO_full: in     vl_logic;
        outDownLeftFIFO_din: out    vl_logic_vector;
        outDownLeftFIFO_wrreq: out    vl_logic;
        outDownRightFIFO_full: in     vl_logic;
        outDownRightFIFO_din: out    vl_logic_vector;
        outDownRightFIFO_wrreq: out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of ADDRESS_FILTER_START : constant is 1;
    attribute mti_svvh_generic_type of ADDRESS_FILTER_STOP : constant is 1;
    attribute mti_svvh_generic_type of ADDRESS_FILTER : constant is 1;
    attribute mti_svvh_generic_type of GLOBAL_CELL_BITS : constant is 1;
    attribute mti_svvh_generic_type of DENDRITE_BITS : constant is 1;
    attribute mti_svvh_generic_type of FIFO_WIDTH_BITS : constant is 1;
end arbiter;
