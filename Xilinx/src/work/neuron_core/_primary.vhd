library verilog;
use verilog.vl_types.all;
entity neuron_core is
    generic(
        ADDRESS_FILTER  : vl_logic := Hi0;
        GLOBAL_CELL_BITS: integer := 6;
        CELLS           : integer := 16;
        CELL_BITS       : integer := 4;
        SYNAPSE_BITS    : integer := 2;
        DENDRITE_BITS   : integer := 2;
        FIFO_WIDTH_BITS : integer := 24;
        VMEM_BITS       : integer := 12
    );
    port(
        rst             : in     vl_logic;
        clk             : in     vl_logic;
        processCell     : in     vl_logic;
        synapseType     : in     vl_logic;
        inDistantFIFO_full: out    vl_logic;
        inDistantFIFO_din: in     vl_logic_vector;
        inDistantFIFO_wrreq: in     vl_logic;
        outDistantFIFO_empty: out    vl_logic;
        outDistantFIFO_rdreq: in     vl_logic;
        outDistantFIFO_dout: out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of ADDRESS_FILTER : constant is 1;
    attribute mti_svvh_generic_type of GLOBAL_CELL_BITS : constant is 1;
    attribute mti_svvh_generic_type of CELLS : constant is 1;
    attribute mti_svvh_generic_type of CELL_BITS : constant is 1;
    attribute mti_svvh_generic_type of SYNAPSE_BITS : constant is 1;
    attribute mti_svvh_generic_type of DENDRITE_BITS : constant is 1;
    attribute mti_svvh_generic_type of FIFO_WIDTH_BITS : constant is 1;
    attribute mti_svvh_generic_type of VMEM_BITS : constant is 1;
end neuron_core;
