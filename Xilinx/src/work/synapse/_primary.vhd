library verilog;
use verilog.vl_types.all;
entity synapse is
    generic(
        CELLS           : integer := 64;
        CELL_BITS       : integer := 6;
        SYNAPSE_BITS    : integer := 4;
        DENDRITE_BITS   : integer := 4;
        VMEM_BITS       : integer := 12;
        FIFO_WIDTH_BITS : integer := 24
    );
    port(
        rst             : in     vl_logic;
        clk             : in     vl_logic;
        processCell     : in     vl_logic;
        synapseType     : in     vl_logic;
        inDendriteEventFIFO_empty: in     vl_logic;
        inDendriteEventFIFO_rdreq: out    vl_logic;
        inDendriteEventFIFO_dout: in     vl_logic_vector;
        configWeightFIFO_empty: in     vl_logic;
        configWeightFIFO_rdreq: out    vl_logic;
        configWeightFIFO_dout: in     vl_logic_vector;
        spiked          : in     vl_logic_vector;
        refractory      : in     vl_logic_vector;
        cellPspRAM_wradd: out    vl_logic_vector;
        cellPspRAM_din  : out    vl_logic_vector;
        cellPspRAM_wren : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of CELLS : constant is 1;
    attribute mti_svvh_generic_type of CELL_BITS : constant is 1;
    attribute mti_svvh_generic_type of SYNAPSE_BITS : constant is 1;
    attribute mti_svvh_generic_type of DENDRITE_BITS : constant is 1;
    attribute mti_svvh_generic_type of VMEM_BITS : constant is 1;
    attribute mti_svvh_generic_type of FIFO_WIDTH_BITS : constant is 1;
end synapse;
