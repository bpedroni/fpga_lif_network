library verilog;
use verilog.vl_types.all;
entity neuron_processor is
    generic(
        CELLS           : integer := 4;
        CELL_BITS       : integer := 2;
        VMEM_BITS       : integer := 12;
        FIFO_WIDTH_BITS : integer := 24
    );
    port(
        rst             : in     vl_logic;
        clk             : in     vl_logic;
        processCore     : in     vl_logic;
        spiked          : out    vl_logic_vector;
        refractory      : out    vl_logic_vector;
        configCellFIFO_empty: in     vl_logic;
        configCellFIFO_rdreq: out    vl_logic;
        configCellFIFO_dout: in     vl_logic_vector;
        cellPspRAM_rdadd: out    vl_logic_vector;
        cellPspRAM_dout : in     vl_logic_vector;
        spikeEventFIFO_full: in     vl_logic;
        spikeEventFIFO_din: out    vl_logic_vector;
        spikeEventFIFO_wrreq: out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of CELLS : constant is 1;
    attribute mti_svvh_generic_type of CELL_BITS : constant is 1;
    attribute mti_svvh_generic_type of VMEM_BITS : constant is 1;
    attribute mti_svvh_generic_type of FIFO_WIDTH_BITS : constant is 1;
end neuron_processor;
