library verilog;
use verilog.vl_types.all;
entity usb_fifo_control is
    generic(
        IDLE            : vl_logic_vector(2 downto 0) := (Hi0, Hi0, Hi0);
        RECEIVE         : vl_logic_vector(2 downto 0) := (Hi0, Hi0, Hi1);
        PULL            : vl_logic_vector(2 downto 0) := (Hi0, Hi1, Hi0);
        TRANSMIT        : vl_logic_vector(2 downto 0) := (Hi0, Hi1, Hi1);
        RECEIVE_PRECHARGE: vl_logic_vector(2 downto 0) := (Hi1, Hi0, Hi0);
        RECEIVE_PRECHARGE_1: vl_logic_vector(2 downto 0) := (Hi1, Hi0, Hi1);
        TRANSMIT_PRECHARGE: vl_logic_vector(2 downto 0) := (Hi1, Hi1, Hi0)
    );
    port(
        clk_usb         : in     vl_logic;
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        RXF_B           : in     vl_logic;
        RD_B            : out    vl_logic;
        DATA            : inout  vl_logic_vector(7 downto 0);
        TXE_B           : in     vl_logic;
        WR              : out    vl_logic;
        dfu_empty       : out    vl_logic;
        dfu_rdreq       : in     vl_logic;
        dfu_dout        : out    vl_logic_vector(7 downto 0);
        dtu_full        : out    vl_logic;
        dtu_din         : in     vl_logic_vector(7 downto 0);
        dtu_wrreq       : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of IDLE : constant is 2;
    attribute mti_svvh_generic_type of RECEIVE : constant is 2;
    attribute mti_svvh_generic_type of PULL : constant is 2;
    attribute mti_svvh_generic_type of TRANSMIT : constant is 2;
    attribute mti_svvh_generic_type of RECEIVE_PRECHARGE : constant is 2;
    attribute mti_svvh_generic_type of RECEIVE_PRECHARGE_1 : constant is 2;
    attribute mti_svvh_generic_type of TRANSMIT_PRECHARGE : constant is 2;
end usb_fifo_control;
