library verilog;
use verilog.vl_types.all;
entity usb_arbiter_interface is
    generic(
        USB_WIDTH_BITS  : integer := 8;
        FIFO_DISTANT_WIDTH_BITS: integer := 24
    );
    port(
        rst             : in     vl_logic;
        clk             : in     vl_logic;
        processCell_en  : out    vl_logic;
        usbCtrlInFIFO_empty: in     vl_logic;
        usbCtrlInFIFO_rdreq: out    vl_logic;
        usbCtrlInFIFO_dout: in     vl_logic_vector;
        inUsbFIFO_empty : out    vl_logic;
        inUsbFIFO_rdreq : in     vl_logic;
        inUsbFIFO_dout  : out    vl_logic_vector;
        outUsbFIFO_full : out    vl_logic;
        outUsbFIFO_din  : in     vl_logic_vector;
        outUsbFIFO_wrreq: in     vl_logic;
        usbCtrlOutFIFO_full: in     vl_logic;
        usbCtrlOutFIFO_din: out    vl_logic_vector;
        usbCtrlOutFIFO_wrreq: out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of USB_WIDTH_BITS : constant is 1;
    attribute mti_svvh_generic_type of FIFO_DISTANT_WIDTH_BITS : constant is 1;
end usb_arbiter_interface;
