library verilog;
use verilog.vl_types.all;
entity arbiter_top is
    generic(
        GLOBAL_CELL_BITS: integer := 8;
        DENDRITE_BITS   : integer := 5;
        FIFO_WIDTH_BITS : integer := 24
    );
    port(
        rst             : in     vl_logic;
        clk             : in     vl_logic;
        global_time     : in     vl_logic_vector(9 downto 0);
        inUsbFIFO_empty : in     vl_logic;
        inUsbFIFO_rdreq : out    vl_logic;
        inUsbFIFO_dout  : in     vl_logic_vector;
        inUpLeftFIFO_empty: in     vl_logic;
        inUpLeftFIFO_rdreq: out    vl_logic;
        inUpLeftFIFO_dout: in     vl_logic_vector;
        inUpRightFIFO_empty: in     vl_logic;
        inUpRightFIFO_rdreq: out    vl_logic;
        inUpRightFIFO_dout: in     vl_logic_vector;
        outUsbFIFO_full : in     vl_logic;
        outUsbFIFO_din  : out    vl_logic_vector;
        outUsbFIFO_wrreq: out    vl_logic;
        outDownLeftFIFO_full: in     vl_logic;
        outDownLeftFIFO_din: out    vl_logic_vector;
        outDownLeftFIFO_wrreq: out    vl_logic;
        outDownRightFIFO_full: in     vl_logic;
        outDownRightFIFO_din: out    vl_logic_vector;
        outDownRightFIFO_wrreq: out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of GLOBAL_CELL_BITS : constant is 1;
    attribute mti_svvh_generic_type of DENDRITE_BITS : constant is 1;
    attribute mti_svvh_generic_type of FIFO_WIDTH_BITS : constant is 1;
end arbiter_top;
