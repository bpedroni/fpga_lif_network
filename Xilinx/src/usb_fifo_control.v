//////////////////////////////////////////////////////////////////////////////////
// Module Name: USB_fifo_control
//
// Description:
//    This block is designed for USB communication using FT245R chips provided by
//    FTDI chip. Datasheet of FT245R is available on:
//    http://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT245R.pdf
//
//////////////////////////////////////////////////////////////////////////////////

module usb_fifo_control(

   input clk_usb,    // USB clock
   input clk,        // system clock
   input rst,
   
   
   ///////////////////////////
   // USB FIFO (FT245R) I/O //
   ///////////////////////////

   input       RXF_B,
   output reg  RD_B,
   inout [7:0] DATA,
   input       TXE_B,
   output reg  WR,
   
   
   ////////////////////////////////
   // FIFOs between USB and FPGA //
   ////////////////////////////////
   
   // data from USB (dfu) to FPGA
   output       dfu_empty,
	input        dfu_rdreq,
   output [7:0] dfu_dout,
      
   // data to USB (dtu) from FPGA
   output       dtu_full,
	input [7:0]  dtu_din,
   input        dtu_wrreq
   
);


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FT245R pin description
//
// RD_B : Enables the current FIFO data byte from D0...D7 when low.
//        Fetched the next FIFO data byte(if available) from the
//        receive FIFO buffer when RD_B goes from high to low.
//  RXF_B : When high, do not read data from the FIFO. When low,
//          there is data available in the FIFO which can be read by strobing RD_B low,
//          then high again. During reset this signal pin is tri-state.
//  TXE_B : When high, do not write data into the FIFO.
//          When low, data can be written into the FIFO by strobing WR high, then low.
//          During reset this signal pin is tri-state.
//  WR : Writes the data from byte from D0...D7 pins into the transmit FIFO buffer when WR goes from high to low.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

wire dfu_full;
reg dfu_wrreq;
wire [7:0] data_in;

wire dtu_empty;
reg dtu_rdreq;
wire [7:0] dtu_dout;


////////////////
// FPGA FIFOs //
////////////////

fifo_usb FIFO_DATA_FROM_USB (rst, clk_usb, clk, data_in, dfu_wrreq, dfu_rdreq, dfu_dout, dfu_full, dfu_empty);
fifo_usb FIFO_DATA_TO_USB   (rst, clk, clk_usb, dtu_din, dtu_wrreq, dtu_rdreq, dtu_dout, dtu_full, dtu_empty);


/////////////////////////
// TRI-STATE DATA LINE //
/////////////////////////

assign data_in = DATA;
assign DATA = WR? dtu_dout : 8'bz;


/////////////////////////////////
// HANDSHAKING CONTROL SIGNALS //
/////////////////////////////////

reg RXF_B_in, TXE_B_in_1, TXE_B_in_2, WR_in;
wire TXE_B_sync, RxReady, TxReady;

always @(posedge clk_usb)begin
   RXF_B_in <= RXF_B;
   TXE_B_in_1 <= TXE_B;
   TXE_B_in_2 <= TXE_B_in_1;
   WR <= WR_in;
end

assign TXE_B_sync = TXE_B_in_1 | TXE_B_in_2;
assign RxReady    = ~RXF_B_in & ~dfu_full;                  // condition for receiving datum from USB
assign TxReady    = ~dtu_empty & ~TXE_B_sync & RXF_B_in;    // condition for transmitting datum to USB


///////////////////
// STATE MACHINE //
///////////////////

reg [2:0] state;

parameter [2:0] IDLE                = 3'd0;
parameter [2:0] RECEIVE             = 3'd1;
parameter [2:0] PULL                = 3'd2;
parameter [2:0] TRANSMIT            = 3'd3;
parameter [2:0] RECEIVE_PRECHARGE   = 3'd4;
parameter [2:0] RECEIVE_PRECHARGE_1 = 3'd5;
parameter [2:0] TRANSMIT_PRECHARGE  = 3'd6;

always @(posedge rst or posedge clk_usb)
begin
   if(rst)begin
      RD_B      <= 1'b1;
      WR_in     <= 1'b0;
      dfu_wrreq <= 1'b0;
      dtu_rdreq <= 1'b0;
   end else begin
      RD_B      <= ~(state == IDLE && RxReady);
      WR_in     <= (state == PULL);
      dfu_wrreq <= (state == RECEIVE);
      dtu_rdreq <= (state == IDLE && TxReady);
   end
end

always @(posedge rst or posedge clk_usb)
begin
   if(rst)
      state <= IDLE;
   else begin
      case (state)
         IDLE:
         begin
            if (TxReady)
               state <= PULL;
            else if (RxReady)
               state <= RECEIVE;
            else
               state <= state;
         end

         RECEIVE:
         begin
            state <= RECEIVE_PRECHARGE;
         end
         RECEIVE_PRECHARGE:
         begin
            state <= RECEIVE_PRECHARGE_1;
         end
         RECEIVE_PRECHARGE_1:
         begin
            state <= IDLE;
         end
     
         PULL:
         begin
            state <= TRANSMIT;
         end
         TRANSMIT:
         begin
            if(WR)
               state <= TRANSMIT_PRECHARGE;
         end
         TRANSMIT_PRECHARGE:
         begin
            state <= IDLE;
         end

         default:
         begin
            state <= IDLE;
         end
      endcase
   end
end

endmodule
