
module neuron_processor

   #(parameter 
      CELLS=4,
      CELL_BITS=2,
      VMEM_BITS=12,
      FIFO_WIDTH_BITS=24)
   (
      input rst,
      input clk,

      input processCore,
      
      output reg [CELLS-1:0] spiked,
      output [CELLS-1:0] refractory,
      
      
      //////////////////////////////////
      // CELL PARAMETER CONFIGURATION //
      //////////////////////////////////
      
      input                         configCellFIFO_empty,
      output reg                    configCellFIFO_rdreq,
      input [2*FIFO_WIDTH_BITS-1:0] configCellFIFO_dout,

      
      /////////////
      // PSP RAM //
      /////////////
      
      output [CELL_BITS-1:0]        cellPspRAM_rdadd,
      input [VMEM_BITS-1:0]         cellPspRAM_dout,

        
      //////////////////////////
      // OUTGOING SPIKE EVENT //
      //////////////////////////
      
      input                            spikeEventFIFO_full,
      output reg [FIFO_WIDTH_BITS-1:0] spikeEventFIFO_din,
      output reg                       spikeEventFIFO_wrreq

   );


   
reg [CELL_BITS-1:0] cell_being_processed;
reg move_to_next_cell;
reg cell_being_processed_done;

// user-defined constants
reg signed [VMEM_BITS-1:0]  neuron_Vrest;          // resting potential
reg [VMEM_BITS-1:0]         neuron_alpha;          // leak term
wire signed [VMEM_BITS-1:0] neuron_bias;           // constant driving current
reg signed [VMEM_BITS-1:0]  neuron_threshold;      // neuron_threshold
reg signed [VMEM_BITS-1:0]  neuron_Vreset;         // reset potential
reg [VMEM_BITS-1:0]         neuron_Tref;           // refractory period	

reg                         neuron_spikeProbe;     // flag to send out spike events to USB
reg                         neuron_VmemProbe;      // flag to send out Vmem values to USB

// neuron cell variables
reg signed [VMEM_BITS-1:0] Vmem;
reg signed [VMEM_BITS-1:0] leak;


/////////////////
// Assignments //
/////////////////

assign cellPspRAM_rdadd   = cell_being_processed;

// select spike event or spike/Vmem probe to USB
reg [1:0] spike_event_select;
always @(*)
begin
   case(spike_event_select)
      0: 
      begin
         spikeEventFIFO_din[FIFO_WIDTH_BITS-1:CELL_BITS] = 0;
         spikeEventFIFO_din[CELL_BITS-1:0] = cell_being_processed;
      end   
      1: 
      begin
         spikeEventFIFO_din[FIFO_WIDTH_BITS-1:FIFO_WIDTH_BITS-3] = 3'b100;
         spikeEventFIFO_din[FIFO_WIDTH_BITS-4:CELL_BITS] = 0;
         spikeEventFIFO_din[CELL_BITS-1:0] = cell_being_processed;
      end
      2:
      begin
         spikeEventFIFO_din[FIFO_WIDTH_BITS-1:FIFO_WIDTH_BITS-3] = 3'b110;
         spikeEventFIFO_din[FIFO_WIDTH_BITS-4:CELL_BITS] = 0;
         spikeEventFIFO_din[CELL_BITS-1:0] = cell_being_processed;
      end
      3:
      begin
         spikeEventFIFO_din[FIFO_WIDTH_BITS-1:FIFO_WIDTH_BITS-3] = 3'b111;
         spikeEventFIFO_din[FIFO_WIDTH_BITS-4:VMEM_BITS] = 0;
         spikeEventFIFO_din[VMEM_BITS-1:0] = Vmem;
      end
   endcase
end


wire [3:0]           parameter_code  = configCellFIFO_dout[23:20];
wire [VMEM_BITS-1:0] parameter_value = configCellFIFO_dout[VMEM_BITS-1:0];
wire [CELL_BITS-1:0] parameter_addr  = configCellFIFO_dout[31+CELL_BITS:32];


//////////////////////
// BIAS & VMEM RAMs //
//////////////////////

// used during neuron processing
wire [CELL_BITS-1:0] cellBiasRAM_rdadd = cell_being_processed; 
wire [VMEM_BITS-1:0] cellBiasRAM_dout;
// used during configuration
wire [CELL_BITS-1:0] cellBiasRAM_wraddr = parameter_addr;
wire [VMEM_BITS-1:0] cellBiasRAM_din    = parameter_value;
reg cellBiasRAM_wren;

ram_16x12bits BIAS_RAM
	(cellBiasRAM_wraddr, cellBiasRAM_din, cellBiasRAM_rdadd, clk, cellBiasRAM_wren, , cellBiasRAM_dout);

wire [CELL_BITS-1:0] cellVmemRAM_rdadd = cell_being_processed; 
wire [VMEM_BITS-1:0] cellVmemRAM_dout;
wire [CELL_BITS-1:0] cellVmemRAM_wraddr = cell_being_processed;
wire [VMEM_BITS-1:0] cellVmemRAM_din = Vmem;
reg cellVmemRAM_wren;

ram_16x12bits VMEM_RAM
   (cellVmemRAM_wraddr, cellVmemRAM_din, cellVmemRAM_rdadd, clk, cellVmemRAM_wren, , cellVmemRAM_dout);


/////////////////////////////////////
// Neuron processing state machine //
/////////////////////////////////////

reg [3:0] curr_state;
reg [3:0] next_state;

parameter [3:0] STATE_RESET                     = 4'd00;
parameter [3:0] STATE_IDLE                      = 4'd01;
parameter [3:0] STATE_CONFIGURE_PARAMETER       = 4'd02;
parameter [3:0] STATE_VERIFY_REFRACTORY         = 4'd03;
parameter [3:0] STATE_RESTORE_VMEM_BIAS_PSP     = 4'd04;
parameter [3:0] STATE_BEGIN_PROCESSING_CELL     = 4'd05;
parameter [3:0] STATE_LEAK_ADJUST               = 4'd06;
parameter [3:0] STATE_APPLY_LEAK                = 4'd07;
parameter [3:0] STATE_APPLY_BIAS                = 4'd08;
parameter [3:0] STATE_APPLY_SYN                 = 4'd09;
parameter [3:0] STATE_APPLY_THRESHOLD           = 4'd10;
parameter [3:0] STATE_SPIKE                     = 4'd11;
parameter [3:0] STATE_SPIKE_PROBE               = 4'd12;
parameter [3:0] STATE_VMEM_PROBE1               = 4'd13;
parameter [3:0] STATE_VMEM_PROBE2               = 4'd14;
parameter [3:0] STATE_REFRACTORY_AND_WRITE_VMEM = 4'd15;
parameter [3:0] STATE_NEXT_CELL                 = 4'd16;


// configure neuron parameters
always @(posedge rst or posedge clk)
begin
   if(rst) begin
      neuron_alpha     <= 3;
      cellBiasRAM_wren <= 1'b0;
      neuron_Vrest     <= 0;
      neuron_threshold <= 64;
      neuron_Vreset    <= 0;
      neuron_Tref      <= 1;
   end else if(curr_state == STATE_CONFIGURE_PARAMETER)begin
      if(parameter_code == 4'd0)
         neuron_alpha <= parameter_value;
      
      else if(parameter_code == 4'd1)
         cellBiasRAM_wren <= 1'b1;
      
      else if(parameter_code == 4'd2)
         neuron_Vrest <= parameter_value;
      
      else if(parameter_code == 4'd3)
         neuron_threshold <= parameter_value;
      
      else if(parameter_code == 4'd4)
         neuron_Vreset <= parameter_value;
      
      else if(parameter_code == 4'd5)
         neuron_Tref <= parameter_value;
         
      else if(parameter_code == 4'd8)begin
         neuron_spikeProbe <= parameter_value[1];
         neuron_VmemProbe  <= parameter_value[0];
      end
   end else
      cellBiasRAM_wren <= 1'b0;
end

assign neuron_bias = cellBiasRAM_dout;

// leak update
wire signed [VMEM_BITS:0] leak_overflow = (neuron_Vrest - Vmem);
wire signed [VMEM_BITS:0] leak_log = leak_overflow >>> neuron_alpha;
always @(posedge rst or posedge clk)
begin
	if(rst)
		leak <= 0;
      
   else if(curr_state == STATE_LEAK_ADJUST)begin
      // steady-state check
      if(Vmem == neuron_Vrest)
			leak <= 0;
      // if not in steady-state, apply minimum leak
      else if((leak_log <= +1) & (leak_log >= -1))
      begin
			if(Vmem > neuron_Vrest)
				leak <= -1;
			else
				leak <= +1;
      end 
      // otherwise, large leak -> overflow check
      else if(leak_log > ((1<<(VMEM_BITS-1))-1))
         leak <= ((1<<(VMEM_BITS-1))-1);
      else if(leak_log < -(1<<(VMEM_BITS-1)))
         leak <= -(1<<(VMEM_BITS-1));
      else
         leak <= leak_log[VMEM_BITS-1:0];
	end
end

// restore and process Vmem
reg signed [VMEM_BITS:0] Vmem_overflow;
always @(posedge rst or posedge clk)
begin
	if(rst)
		Vmem_overflow <= 0;
   else if(curr_state == STATE_RESTORE_VMEM_BIAS_PSP)
      Vmem_overflow <= cellVmemRAM_dout;
   else if(curr_state == STATE_APPLY_LEAK)
		Vmem_overflow <= Vmem_overflow + leak;
	else if(curr_state == STATE_APPLY_BIAS)
		Vmem_overflow <= Vmem_overflow + neuron_bias;
	else if(curr_state == STATE_APPLY_SYN)
		Vmem_overflow <= Vmem_overflow + cellPspRAM_dout;
	else if(curr_state == STATE_SPIKE)
		Vmem_overflow <= neuron_Vreset;
   else
		Vmem_overflow <= Vmem_overflow;
end

// overflow check
//  note that the overflow is not checked DURING Vmem_overflow processing and 
//  errors may still occur due to the multiple additions in series
always @(*)
begin
   if(Vmem_overflow > ((1<<(VMEM_BITS-1))-1))
      Vmem = ((1<<(VMEM_BITS-1))-1);
   else if(Vmem_overflow < -(1<<(VMEM_BITS-1)))
      Vmem = -(1<<(VMEM_BITS-1));
   else
      Vmem = Vmem_overflow[VMEM_BITS-1:0];
end

/////////////////
// Stat machine
always @(posedge rst or posedge clk)
begin
	if(rst)
		curr_state <= STATE_RESET;
   else
		curr_state <= next_state;
end

always @(*)
begin
   move_to_next_cell    = 0;
   configCellFIFO_rdreq = 0;
   spikeEventFIFO_wrreq = 0;
   cellVmemRAM_wren     = 0;
   spike_event_select   = 0;
   
	next_state = curr_state;

	case(curr_state)
		STATE_RESET:
		begin
			next_state = STATE_IDLE;
		end
		STATE_IDLE:
		begin
         if(~configCellFIFO_empty)begin
            configCellFIFO_rdreq = 1;
            next_state = STATE_CONFIGURE_PARAMETER;
         end else if(~cell_being_processed_done)
            next_state = STATE_VERIFY_REFRACTORY;
		end

      STATE_CONFIGURE_PARAMETER:
      begin
         next_state = STATE_IDLE;
      end
      
      STATE_VERIFY_REFRACTORY:
      begin
         if(refractory[cell_being_processed] == 0)
            next_state = STATE_RESTORE_VMEM_BIAS_PSP;
         else begin
            move_to_next_cell = 1;
            next_state = STATE_NEXT_CELL;
         end
      end
      STATE_RESTORE_VMEM_BIAS_PSP:
      begin
         next_state = STATE_BEGIN_PROCESSING_CELL;
      end
      STATE_BEGIN_PROCESSING_CELL:
      begin
         next_state = STATE_LEAK_ADJUST;
      end
      STATE_LEAK_ADJUST:
		begin
				next_state = STATE_APPLY_LEAK;
		end
		STATE_APPLY_LEAK:
		begin
			next_state = STATE_APPLY_BIAS;
		end
      STATE_APPLY_BIAS:
		begin
			next_state = STATE_APPLY_SYN;
		end
		STATE_APPLY_SYN:
		begin
			next_state = STATE_APPLY_THRESHOLD;
		end
		STATE_APPLY_THRESHOLD:
		begin
         if(Vmem > neuron_threshold)
				next_state = STATE_SPIKE;
			else if(neuron_VmemProbe)
            next_state = STATE_VMEM_PROBE1;
         else
            next_state = STATE_REFRACTORY_AND_WRITE_VMEM;
		end
		STATE_SPIKE:
		begin
			if(~spikeEventFIFO_full)begin
            spikeEventFIFO_wrreq = 1;
				
            if(neuron_spikeProbe)
               next_state = STATE_SPIKE_PROBE;
            else if(neuron_VmemProbe)
               next_state = STATE_VMEM_PROBE1;
            else
               next_state = STATE_REFRACTORY_AND_WRITE_VMEM;
         end
		end
      STATE_SPIKE_PROBE:
      begin
         spike_event_select = 1;
         if(~spikeEventFIFO_full)begin
            spikeEventFIFO_wrreq = 1;
            if(neuron_VmemProbe)
               next_state = STATE_VMEM_PROBE1;
            else
               next_state = STATE_REFRACTORY_AND_WRITE_VMEM;
         end
      end
      STATE_VMEM_PROBE1:
      begin
         spike_event_select = 2;
         if(~spikeEventFIFO_full)begin
            spikeEventFIFO_wrreq = 1;
            next_state = STATE_VMEM_PROBE2;
         end
      end
      STATE_VMEM_PROBE2:
      begin
         spike_event_select = 3;
         if(~spikeEventFIFO_full)begin
            spikeEventFIFO_wrreq = 1;
            next_state = STATE_REFRACTORY_AND_WRITE_VMEM;
         end
      end      
      STATE_REFRACTORY_AND_WRITE_VMEM:
      begin
         cellVmemRAM_wren = 1;
         move_to_next_cell = 1;
         next_state = STATE_NEXT_CELL;
      end
      STATE_NEXT_CELL:
		begin
			next_state = STATE_IDLE;
		end
      
		default:
		begin
         move_to_next_cell    = 0;
         configCellFIFO_rdreq = 0;
         spikeEventFIFO_wrreq = 0;
         cellVmemRAM_wren     = 0;
         spike_event_select   = 0;
         next_state = STATE_RESET;
		end
	endcase
end

/////////////////////////////
// Cycle through all cells //
/////////////////////////////

always @(posedge rst or posedge clk)
begin
   if(rst)begin
      cell_being_processed <= 0;
      cell_being_processed_done <= 0;
   end else if(processCore)begin
      cell_being_processed <= 0;
      cell_being_processed_done <= 0;
   end else if(move_to_next_cell)begin
      if(cell_being_processed < (CELLS-1))
         cell_being_processed <= cell_being_processed + 1;
      else
         cell_being_processed_done <= 1;
   end
end

//////////////////////////
// Update spiked states //
//////////////////////////

always @(posedge rst or posedge clk)
begin
   if(rst)
      spiked <= 0;
   else if(processCore)
      spiked <= 0;
   else if(curr_state == STATE_SPIKE)
      spiked[cell_being_processed] <= 1'b1;
end

//////////////////////////////
// Update refractory states //
//////////////////////////////
reg [VMEM_BITS-1:0] refractory_state [CELLS-1:0];
genvar i;

generate
   for (i=0; i<CELLS; i=i+1) begin: gen1
      always @(posedge rst or posedge clk)
      begin
         if(rst)
            refractory_state[i] <= 0;
         else if(processCore)begin
            if(refractory_state[i] > 0)
               refractory_state[i] <= refractory_state[i] - 1;
         end else if((cell_being_processed==i) & (curr_state==STATE_SPIKE))
            refractory_state[i] <= neuron_Tref;
      end
   end
endgenerate

// Output refractory signal
genvar k;

generate
   for (k=0; k<CELLS; k=k+1) begin: gen2
      assign refractory[k] = (refractory_state[k] > 0);
   end
endgenerate

endmodule
