
module controller_usb 
   #(parameter 
      USB_DEPTH_BITS=5,
      USB_WIDTH_BITS=8,
      FIFO_USB_WIDTH_BITS=16) 
   (
      input rst,
      input clk,

      ///////////////////////
      // INCOMING USB PATH //
      ///////////////////////
      
      input [USB_WIDTH_BITS-1:0]       usb_data,
      input                            usb_req,
      output reg                       usb_ack,

      output                           inUsbFIFO_empty,
      input                            inUsbFIFO_rdreq,
      output [FIFO_USB_WIDTH_BITS-1:0] inUsbFIFO_dout,
      
      
      ///////////////////////
      // OUTGOING USB PATH //
      ///////////////////////
      
      output                          outUsbFIFO_full,
      input [FIFO_USB_WIDTH_BITS-1:0] outUsbFIFO_din,
      input                           outUsbFIFO_wrreq,

      output [USB_WIDTH_BITS-1:0]     usbout_data,
      output reg                      usbout_req,
      input                           usbout_ack
      
   );


///////////////
// USB FIFOs //
///////////////

reg inUsbFIFO_wrreq;
wire inUsbFIFO_full;

ip_dcfifo_mixed_widths #(USB_DEPTH_BITS, USB_WIDTH_BITS, FIFO_USB_WIDTH_BITS) USB_FIFO_IN 
   (rst, usb_data, clk, inUsbFIFO_rdreq, clk, inUsbFIFO_wrreq, inUsbFIFO_dout, inUsbFIFO_empty, inUsbFIFO_full);

reg outUsbFIFO_rdreq;
wire outUsbFIFO_empty;

ip_dcfifo_mixed_widths #(USB_DEPTH_BITS, FIFO_USB_WIDTH_BITS, USB_WIDTH_BITS) USB_FIFO_OUT
   (rst, outUsbFIFO_din, clk, outUsbFIFO_rdreq, clk, outUsbFIFO_wrreq, usbout_data, outUsbFIFO_empty, outUsbFIFO_full);

   
////////////////////////////////////
// USB RECEIVE DATA STATE MACHINE //
////////////////////////////////////

reg [1:0] curr_state;
reg [1:0] next_state;

parameter [1:0] STATE_RESET       = 2'd0;
parameter [1:0] STATE_IDLE        = 2'd1;
parameter [1:0] STATE_RX_USB_DATA = 2'd2;

always @(posedge rst or posedge clk)
begin
	if(rst)
		curr_state <= STATE_RESET;
   else
		curr_state <= next_state;
end

always @(*)
begin
   inUsbFIFO_wrreq = 0;
   usb_ack         = 0;
   
   next_state = curr_state;

	case(curr_state)
		STATE_RESET:
		begin
         next_state = STATE_IDLE;
		end
		STATE_IDLE:
		begin
			if(usb_req & ~inUsbFIFO_full)begin
            inUsbFIFO_wrreq = 1;
            next_state = STATE_RX_USB_DATA;
         end
      end
      STATE_RX_USB_DATA:
      begin
         usb_ack = 1;
         if(~usb_req)
            next_state = STATE_IDLE;
      end
      default:
      begin
         usb_ack         = 0;
         inUsbFIFO_wrreq = 0;
         next_state = STATE_RESET;
      end
   endcase
end


/////////////////////////////////////
// USB TRANSMIT DATA STATE MACHINE //
/////////////////////////////////////

reg [1:0] curr_state_tx;
reg [1:0] next_state_tx;

parameter [1:0] STATE_TX_RESET    = 2'd0;
parameter [1:0] STATE_TX_IDLE     = 2'd1;
parameter [1:0] STATE_TX_USB_DATA = 2'd2;

always @(posedge rst or posedge clk)
begin
	if(rst)
		curr_state_tx <= STATE_TX_RESET;
   else
		curr_state_tx <= next_state_tx;
end

always @(*)
begin
   outUsbFIFO_rdreq = 0;
   usbout_req       = 0;
   
   next_state_tx = curr_state_tx;

	case(curr_state_tx)
		STATE_TX_RESET:
		begin
         next_state_tx = STATE_TX_IDLE;
		end
		STATE_TX_IDLE:
		begin
			if(~outUsbFIFO_empty & ~usbout_ack)begin
            outUsbFIFO_rdreq = 1;
            next_state_tx = STATE_TX_USB_DATA;
         end
      end
      STATE_TX_USB_DATA:
      begin
         usbout_req = 1;
         if(usbout_ack)
            next_state_tx = STATE_TX_IDLE;
      end
      default:
      begin
         usbout_req       = 0;
         outUsbFIFO_rdreq = 0;
         next_state_tx = STATE_TX_RESET;
      end
   endcase
end


endmodule
