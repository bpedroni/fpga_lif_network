// Cyclone IV FIFO IP
module ip_dcfifo #(parameter DEPTH_BITS=5,WIDTH_BITS=8)(
	clock,
	data,
	rdreq,
	sclr,
	wrreq,
	empty,
	full,
	q);

	input	  clock;
	input	[WIDTH_BITS-1:0]  data;
	input	  rdreq;
	input	  sclr;
	input	  wrreq;
	output	  empty;
	output	  full;
	output	[WIDTH_BITS-1:0]  q;

	wire  sub_wire0;
	wire  sub_wire1;
	wire [WIDTH_BITS-1:0] sub_wire2;
	wire  empty = sub_wire0;
	wire  full = sub_wire1;
	wire [WIDTH_BITS-1:0] q = sub_wire2[WIDTH_BITS-1:0];

	scfifo	scfifo_component (
				.clock (clock),
				.data (data),
				.rdreq (rdreq),
				.sclr (sclr),
				.wrreq (wrreq),
				.empty (sub_wire0),
				.full (sub_wire1),
				.q (sub_wire2),
				.aclr (),
				.almost_empty (),
				.almost_full (),
				.usedw ());
	defparam
		scfifo_component.add_ram_output_register = "OFF",
		scfifo_component.intended_device_family = "Cyclone IV E",
		scfifo_component.lpm_numwords = 1 << DEPTH_BITS,
		scfifo_component.lpm_showahead = "OFF",
		scfifo_component.lpm_type = "scfifo",
		scfifo_component.lpm_width = WIDTH_BITS,
		scfifo_component.lpm_widthu = DEPTH_BITS,
		scfifo_component.overflow_checking = "OFF",
		scfifo_component.underflow_checking = "OFF",
		scfifo_component.use_eab = "ON";

endmodule
