# README #

The Verilog code present in this repository is intended for use with Altera or Xilinx FPGAs and should be compiled and synthesized using, respectively, Quartus II or Xilinx ISE (Vivado).

All simulations were realized using Modelsim.

### What is this repository for? ###

* Configurable neural network of leaky integrate-and-fire (LIF) neurons.
* Real-time operation, with 1 ms time-scale.
* Distributed RAM implements local core configurations.
* Hierarchical routing architecture (binary tree).
* Spike timing dependent plasticity (STDP) as learning rule.

### How do I get set up? ###

* Compile Verilog code using Quartus II or Xilinx ISE.
* Physical implementation of this project is being done on Xilinx Spartan 6 XC6SLX150 FPGA board.
* Command list included in Documentation.xlsx.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Bruno U. Pedroni: bpedroni@eng.ucsd.edu


## Additional documentation ###
* Design strategies

http://rtlery.com/articles/how-design-round-robin-arbiter
http://www.xilinx.com/support/documentation/application_notes/xapp522-mux-design-techniques.pdf
http://idlelogiclabs.com/2012/04/15/talking-to-the-de0-nano-using-the-virtual-jtag-interface/

* Altera

https://www.altera.com/en_US/pdfs/literature/hb/cyclone-iv/cyclone4-handbook.pdf

* Xilinx

http://www.xilinx.com/products/intellectual-property/block_memory_generator.html
http://www.xilinx.com/publications/prod_mktg/Spartan6_Product_Table.pdf