
module arbiter_top 
   #(parameter 
      GLOBAL_CELL_BITS=8,
      DENDRITE_BITS=5,
      FIFO_DEPTH_BITS=4,
      FIFO_DISTANT_WIDTH_BITS=16)
   (
      input rst,
      input clk,
      

      /////////////////////
      // INCOMING EVENTS //
      /////////////////////
      
      input                               inUsbFIFO_empty,
      output reg                          inUsbFIFO_rdreq,
      input [FIFO_DISTANT_WIDTH_BITS-1:0] inUsbFIFO_dout,
      
      input                               inUpLeftFIFO_empty,
      output reg                          inUpLeftFIFO_rdreq,
      input [FIFO_DISTANT_WIDTH_BITS-1:0] inUpLeftFIFO_dout,
      
      input                               inUpRightFIFO_empty,
      output reg                          inUpRightFIFO_rdreq,
      input [FIFO_DISTANT_WIDTH_BITS-1:0] inUpRightFIFO_dout,
      
      
      /////////////////////
      // OUTGOING EVENTS //
      /////////////////////

      input                                outUsbFIFO_full,
      output [FIFO_DISTANT_WIDTH_BITS-1:0] outUsbFIFO_din,
      output reg                           outUsbFIFO_wrreq,
      
      input                                outDownLeftFIFO_full,
      output [FIFO_DISTANT_WIDTH_BITS-1:0] outDownLeftFIFO_din,
      output reg                           outDownLeftFIFO_wrreq,
      
      input                                outDownRightFIFO_full,
      output [FIFO_DISTANT_WIDTH_BITS-1:0] outDownRightFIFO_din,
      output reg                           outDownRightFIFO_wrreq
      );

reg move_to_next_path;
reg [1:0] incoming_path_being_serviced;


/////////////////////////////////
// Incoming path arbitration
always @(posedge rst or posedge clk)
begin
   if(rst)
      incoming_path_being_serviced <= 0;
   else if(move_to_next_path)begin
      if(incoming_path_being_serviced < 2)
         incoming_path_being_serviced <= incoming_path_being_serviced + 1;
      else
         incoming_path_being_serviced <= 0;
   end
end

// incoming empty
wire [2:0] mux_empty = {inUpRightFIFO_empty, inUpLeftFIFO_empty, inUsbFIFO_empty};
wire incomingPath_empty = mux_empty[incoming_path_being_serviced];

// incoming rdreq and dout
reg incomingPath_rdreq;
reg [FIFO_DISTANT_WIDTH_BITS-1:0] incomingPath_dout;

always @(*)begin
   inUsbFIFO_rdreq     = 0;
   inUpLeftFIFO_rdreq  = 0;
   inUpRightFIFO_rdreq = 0;
   
   incomingPath_dout = 0;
   
   case(incoming_path_being_serviced)
      0: begin
         inUsbFIFO_rdreq   = incomingPath_rdreq;
         incomingPath_dout = inUsbFIFO_dout;
      end
      1: begin
         inUpLeftFIFO_rdreq = incomingPath_rdreq;
         incomingPath_dout  = inUpLeftFIFO_dout;
      end
      2: begin
         inUpRightFIFO_rdreq = incomingPath_rdreq;
         incomingPath_dout   = inUpRightFIFO_dout;
      end 
      default: begin
         inUsbFIFO_rdreq     = 0;
         inUpLeftFIFO_rdreq  = 0;
         inUpRightFIFO_rdreq = 0;
         incomingPath_dout   = 0;
      end
   endcase
end


/////////////////////////////////////////////////////////
// Output data is written based on state machine (wrreq)
assign outUsbFIFO_din       = incomingPath_dout;
assign outDownLeftFIFO_din  = incomingPath_dout;
assign outDownRightFIFO_din = incomingPath_dout;


/////////////////////////////////////
// state machine: servicing event
parameter [3:0] STATE_RESET                       = 4'd00;
parameter [3:0] STATE_IDLE                        = 4'd01;
parameter [3:0] STATE_VERIFY_EVENT_TYPE           = 4'd02;
parameter [3:0] STATE_SEND_SPIKE_OUT              = 4'd03;
parameter [3:0] STATE_ROUTE_CONFIG_DOWN_LEFT1     = 4'd04;
parameter [3:0] STATE_READ_NEXT_CONFIG_WORD_LEFT  = 4'd05;
parameter [3:0] STATE_ROUTE_CONFIG_DOWN_LEFT2     = 4'd06;
parameter [3:0] STATE_ROUTE_CONFIG_DOWN_RIGHT1    = 4'd07;
parameter [3:0] STATE_READ_NEXT_CONFIG_WORD_RIGHT = 4'd08;
parameter [3:0] STATE_ROUTE_CONFIG_DOWN_RIGHT2    = 4'd09;
parameter [3:0] STATE_VERIFY_ADDRESS_FILTER       = 4'd10;
parameter [3:0] STATE_ROUTE_DOWN_LEFT             = 4'd11;
parameter [3:0] STATE_ROUTE_DOWN_RIGHT            = 4'd12;

reg [3:0] curr_state;
reg [3:0] next_state;

always @(posedge rst or posedge clk)
begin
   if(rst)
      curr_state <= STATE_RESET;
   else
      curr_state <= next_state;
end

always @(*)
begin

   move_to_next_path      = 0;
   incomingPath_rdreq     = 0;
   outUsbFIFO_wrreq       = 0;
   outDownLeftFIFO_wrreq  = 0;
   outDownRightFIFO_wrreq = 0;
      
   next_state = curr_state;

   case(curr_state)
      STATE_RESET:
      begin
         next_state = STATE_IDLE;
      end
      
      STATE_IDLE:
      begin
         if(~incomingPath_empty)begin
            incomingPath_rdreq = 1;
            next_state = STATE_VERIFY_EVENT_TYPE;
         end else
            move_to_next_path = 1;
      end
      
      // configuration events can only go down; spike out events can only go up
      STATE_VERIFY_EVENT_TYPE:
      begin
         if(incomingPath_dout[FIFO_DISTANT_WIDTH_BITS-1])begin     // MSB=1: configuration event OR spike out event
            if(incoming_path_being_serviced > 0)                   // spike out event: send up
               next_state = STATE_SEND_SPIKE_OUT;
            else if(~incomingPath_dout[FIFO_DISTANT_WIDTH_BITS-2]) // configuration event: if left configuration event
               next_state = STATE_ROUTE_CONFIG_DOWN_LEFT1;
            else                                                   // if right configuration event
               next_state = STATE_ROUTE_CONFIG_DOWN_RIGHT1;
         end
         else
            next_state = STATE_VERIFY_ADDRESS_FILTER;
      end
      
      // spike out event
      STATE_SEND_SPIKE_OUT:
      begin
         if(~outUsbFIFO_full)begin
            outUsbFIFO_wrreq = 1;
            next_state = STATE_IDLE;
         end
      end
      
      ////////////
      STATE_ROUTE_CONFIG_DOWN_LEFT1:
      begin
         if(~outDownLeftFIFO_full)begin
            outDownLeftFIFO_wrreq = 1;
            next_state = STATE_READ_NEXT_CONFIG_WORD_LEFT;
         end
      end
      STATE_READ_NEXT_CONFIG_WORD_LEFT:
      begin
         if(~incomingPath_empty)begin
            incomingPath_rdreq = 1;
            next_state = STATE_ROUTE_CONFIG_DOWN_LEFT2;
         end
      end
      STATE_ROUTE_CONFIG_DOWN_LEFT2:
      begin
         if(~outDownLeftFIFO_full)begin
            outDownLeftFIFO_wrreq = 1;
            move_to_next_path = 1;
            next_state = STATE_IDLE;
         end
      end
      
      STATE_ROUTE_CONFIG_DOWN_RIGHT1:
      begin
         if(~outDownRightFIFO_full)begin
            outDownRightFIFO_wrreq = 1;
            next_state = STATE_READ_NEXT_CONFIG_WORD_RIGHT;
         end
      end
      STATE_READ_NEXT_CONFIG_WORD_RIGHT:
      begin
         if(~incomingPath_empty)begin
            incomingPath_rdreq = 1;
            next_state = STATE_ROUTE_CONFIG_DOWN_RIGHT2;
         end
      end
      STATE_ROUTE_CONFIG_DOWN_RIGHT2:
      begin
         if(~outDownRightFIFO_full)begin
            outDownRightFIFO_wrreq = 1;
            move_to_next_path = 1;
            next_state = STATE_IDLE;
         end
      end
      
      // verify if should go left (0) or right (1)
      STATE_VERIFY_ADDRESS_FILTER:
      begin
         if(~incomingPath_dout[GLOBAL_CELL_BITS+DENDRITE_BITS-1])
            next_state = STATE_ROUTE_DOWN_LEFT;
         else
            next_state = STATE_ROUTE_DOWN_RIGHT;
      end
      
      STATE_ROUTE_DOWN_LEFT:
      begin
         if(~outDownLeftFIFO_full)begin
            outDownLeftFIFO_wrreq = 1;
            move_to_next_path = 1;
            next_state = STATE_IDLE;
         end
      end
      
      STATE_ROUTE_DOWN_RIGHT:
      begin
         if(~outDownRightFIFO_full)begin
            outDownRightFIFO_wrreq = 1;
            move_to_next_path = 1;
            next_state = STATE_IDLE;
         end
      end

      default:
      begin
         move_to_next_path      = 0;
         incomingPath_rdreq     = 0;
         outUsbFIFO_wrreq       = 0;
         outDownLeftFIFO_wrreq  = 0;
         outDownRightFIFO_wrreq = 0;
         next_state = STATE_RESET;
      end
   endcase
end

endmodule

      