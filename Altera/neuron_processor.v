
module neuron_processor

   #(parameter 
      CELLS=4,
      CELL_BITS=2,
      VMEM_BITS=12,
      RAM_DEPTH_BITS=CELL_BITS,
      RAM_WIDTH_BITS=VMEM_BITS)
   (
      input rst,
      input clk,

      input processCore,
      
      output [CELLS-1:0] refractory,
      
      
      ///////////////////////////
      // NEURON PARAMETERS RAM //
      ///////////////////////////
      
      input        configParamFIFO_empty,
      output reg   configParamFIFO_rdreq,
      input [31:0] configParamFIFO_dout,


      /////////////
      // PSP RAM //
      /////////////
      
      output [CELL_BITS-1:0] cellPspRAM_rdadd,
      input [VMEM_BITS-1:0]  cellPspRAM_dout,

        
      //////////////////////////
      // OUTGOING SPIKE EVENT //
      //////////////////////////
      
      input                  spikeEventFIFO_full,
      output [CELL_BITS-1:0] spikeEventFIFO_din,
      output reg             spikeEventFIFO_wrreq

   );


   
reg [CELL_BITS-1:0] cell_being_processed;
reg move_to_next_cell;
reg cell_being_processed_done;

// user-defined constants
reg signed [VMEM_BITS-1:0]  neuron_Vrest;          // resting potential
reg [VMEM_BITS-1:0]         neuron_alpha;          // leak term
wire signed [VMEM_BITS-1:0] neuron_bias;           // constant driving current
reg signed [VMEM_BITS-1:0]  neuron_threshold;      // neuron_threshold
reg signed [VMEM_BITS-1:0]  neuron_Vreset;         // reset potential
reg [VMEM_BITS-1:0]         neuron_Tref;           // refractory period	

// neuron cell variables
reg signed [VMEM_BITS-1:0] Vmem;
reg signed [VMEM_BITS-1:0] leak;


/////////////////
// Assignments //
/////////////////

assign cellPspRAM_rdadd   = cell_being_processed;
assign spikeEventFIFO_din = cell_being_processed;

wire [2:0]           parameter_code  = configParamFIFO_dout[31:29];
wire [VMEM_BITS-1:0] parameter_value = configParamFIFO_dout[15+VMEM_BITS:16];
wire [CELL_BITS-1:0] parameter_addr  = configParamFIFO_dout[CELL_BITS-1:0];


//////////////////////
// BIAS & VMEM RAMs //
//////////////////////

// used during neuron processing
wire [CELL_BITS-1:0] cellBiasRAM_rdadd = cell_being_processed; 
wire [VMEM_BITS-1:0] cellBiasRAM_dout;
// used during configuration
wire [CELL_BITS-1:0] cellBiasRAM_wraddr = parameter_addr;
wire [VMEM_BITS-1:0] cellBiasRAM_din    = parameter_value;
reg cellBiasRAM_wren;

ip_ddr #(RAM_DEPTH_BITS, RAM_WIDTH_BITS) BIAS_RAM
   (rst, clk, cellBiasRAM_din, cellBiasRAM_rdadd, cellBiasRAM_wraddr, cellBiasRAM_wren, cellBiasRAM_dout);

wire [CELL_BITS-1:0] cellVmemRAM_rdadd = cell_being_processed; 
wire [VMEM_BITS-1:0] cellVmemRAM_dout;
wire [CELL_BITS-1:0] cellVmemRAM_wraddr = cell_being_processed;
wire [VMEM_BITS-1:0] cellVmemRAM_din = Vmem;
reg cellVmemRAM_wren;

ip_ddr #(RAM_DEPTH_BITS, RAM_WIDTH_BITS) VMEM_RAM
   (rst, clk, cellVmemRAM_din, cellVmemRAM_rdadd, cellVmemRAM_wraddr, cellVmemRAM_wren, cellVmemRAM_dout);
   

/////////////////////////////////////
// Neuron processing state machine //
/////////////////////////////////////

reg [3:0] curr_state;
reg [3:0] next_state;

parameter [3:0] STATE_RESET                     = 4'd00;
parameter [3:0] STATE_IDLE                      = 4'd01;
parameter [3:0] STATE_CONFIGURE_PARAMETER       = 4'd02;
parameter [3:0] STATE_VERIFY_REFRACTORY         = 4'd03;
parameter [3:0] STATE_RESTORE_VMEM_BIAS_PSP     = 4'd04;
parameter [3:0] STATE_BEGIN_PROCESSING_CELL     = 4'd05;
parameter [3:0] STATE_LEAK_ADJUST               = 4'd06;
parameter [3:0] STATE_APPLY_LEAK                = 4'd07;
parameter [3:0] STATE_APPLY_BIAS                = 4'd08;
parameter [3:0] STATE_APPLY_SYN                 = 4'd09;
parameter [3:0] STATE_APPLY_THRESHOLD           = 4'd10;
parameter [3:0] STATE_SPIKE                     = 4'd11;
parameter [3:0] STATE_REFRACTORY_AND_WRITE_VMEM = 4'd12;
parameter [3:0] STATE_NEXT_CELL                 = 4'd13;


// configure neuron parameters
always @(posedge rst or posedge clk)
begin
   if(rst) begin
      neuron_alpha     <= 3;
      cellBiasRAM_wren <= 1'b0;
      neuron_Vrest     <= 0;
      neuron_threshold <= 64;
      neuron_Vreset    <= 0;
      neuron_Tref      <= 1;
   end else if(curr_state == STATE_CONFIGURE_PARAMETER)begin
      if(parameter_code == 3'd0)
         neuron_alpha <= parameter_value;
      
      else if(parameter_code == 3'd1)
         cellBiasRAM_wren <= 1'b1;
      
      else if(parameter_code == 3'd2)
         neuron_Vrest <= parameter_value;
      
      else if(parameter_code == 3'd3)
         neuron_threshold <= parameter_value;
      
      else if(parameter_code == 3'd4)
         neuron_Vreset <= parameter_value;
      
      else if(parameter_code == 3'd5)
         neuron_Tref <= parameter_value;
   end else
      cellBiasRAM_wren <= 1'b0;
end

assign neuron_bias = cellBiasRAM_dout;

// leak update
wire [VMEM_BITS-1:0] leak_log = leak >>> neuron_alpha;
always @(posedge rst or posedge clk)
begin
	if(rst)
		leak <= 0;

   else if(curr_state == STATE_BEGIN_PROCESSING_CELL)
		leak <= (neuron_Vrest - Vmem);
      
   else if(curr_state == STATE_LEAK_ADJUST)begin
		if(Vmem == neuron_Vrest)
			leak <= 0;
		else if((leak_log <= +1) & (leak_log >= -1))begin
			if(Vmem > neuron_Vrest)
				leak <= -1;
			else
				leak <= +1;
		end
	end
end

// restore and process Vmem
always @(posedge rst or posedge clk)
begin
	if(rst)
		Vmem <= 0;
   else if(curr_state == STATE_RESTORE_VMEM_BIAS_PSP)
      Vmem <= cellVmemRAM_dout;
   else if(curr_state == STATE_APPLY_LEAK)
		Vmem <= Vmem + leak_log;
	else if(curr_state == STATE_APPLY_BIAS)
		Vmem <= Vmem + neuron_bias;
	else if(curr_state == STATE_APPLY_SYN)
		Vmem <= Vmem + cellPspRAM_dout;
	else if(curr_state == STATE_SPIKE)
		Vmem <= neuron_Vreset;
   else
		Vmem <= Vmem;
end

always @(posedge rst or posedge clk)
begin
	if(rst)
		curr_state <= STATE_RESET;
   else
		curr_state <= next_state;
end

always @(*)
begin
   move_to_next_cell     = 0;
   configParamFIFO_rdreq = 0;
   spikeEventFIFO_wrreq  = 0;
   cellVmemRAM_wren      = 0;

	next_state = curr_state;

	case(curr_state)
		STATE_RESET:
		begin
			next_state = STATE_IDLE;
		end
		STATE_IDLE:
		begin
         if(~configParamFIFO_empty)begin
            configParamFIFO_rdreq = 1;
            next_state = STATE_CONFIGURE_PARAMETER;
         end else if(~cell_being_processed_done)
            next_state = STATE_VERIFY_REFRACTORY;
		end

      STATE_CONFIGURE_PARAMETER:
      begin
         next_state = STATE_IDLE;
      end
      
      STATE_VERIFY_REFRACTORY:
      begin
         if(refractory[cell_being_processed] == 0)
            next_state = STATE_RESTORE_VMEM_BIAS_PSP;
         else begin
            move_to_next_cell = 1;
            next_state = STATE_NEXT_CELL;
         end
      end
      STATE_RESTORE_VMEM_BIAS_PSP:
      begin
         next_state = STATE_BEGIN_PROCESSING_CELL;
      end
      STATE_BEGIN_PROCESSING_CELL:
      begin
         next_state = STATE_LEAK_ADJUST;
      end
      STATE_LEAK_ADJUST:
		begin
				next_state = STATE_APPLY_LEAK;
		end
		STATE_APPLY_LEAK:
		begin
			next_state = STATE_APPLY_BIAS;
		end
      STATE_APPLY_BIAS:
		begin
			next_state = STATE_APPLY_SYN;
		end
		STATE_APPLY_SYN:
		begin
			next_state = STATE_APPLY_THRESHOLD;
		end
		STATE_APPLY_THRESHOLD:
		begin
         if(Vmem > neuron_threshold)
				next_state = STATE_SPIKE;
			else
				next_state = STATE_REFRACTORY_AND_WRITE_VMEM;
		end
		STATE_SPIKE:
		begin
			if(~spikeEventFIFO_full)begin
            spikeEventFIFO_wrreq = 1;
				next_state = STATE_REFRACTORY_AND_WRITE_VMEM;
         end
		end
      STATE_REFRACTORY_AND_WRITE_VMEM:
      begin
         cellVmemRAM_wren = 1;
         move_to_next_cell = 1;
         next_state = STATE_NEXT_CELL;
      end
      STATE_NEXT_CELL:
		begin
			next_state = STATE_IDLE;
		end
      
		default:
		begin
         move_to_next_cell     = 0;
         configParamFIFO_rdreq = 0;
         spikeEventFIFO_wrreq  = 0;
         cellVmemRAM_wren      = 0;
         next_state = STATE_RESET;
		end
	endcase
end

/////////////////////////////
// Cycle through all cells //
/////////////////////////////

always @(posedge rst or posedge clk)
begin
   if(rst)begin
      cell_being_processed <= 0;
      cell_being_processed_done <= 0;
   end else if(processCore)begin
      cell_being_processed <= 0;
      cell_being_processed_done <= 0;
   end else if(move_to_next_cell)begin
      if(cell_being_processed < (CELLS-1))
         cell_being_processed <= cell_being_processed + 1;
      else
         cell_being_processed_done <= 1;
   end
end


//////////////////////////////
// Update refractory states //
//////////////////////////////
reg [VMEM_BITS-1:0] refractory_state [CELLS-1:0];
genvar i;

generate
   for (i=0; i<CELLS; i=i+1) begin: gen1
      always @(posedge rst or posedge clk)
      begin
         if(rst)
            refractory_state[i] <= 0;
         else if(processCore)begin
            if(refractory_state[i] > 0)
               refractory_state[i] <= refractory_state[i] - 1;
         end else if((cell_being_processed==i) & (curr_state==STATE_SPIKE))
            refractory_state[i] <= neuron_Tref;
      end
   end
endgenerate

// Output refractory signal
genvar k;

generate
   for (k=0; k<CELLS; k=k+1) begin: gen2
      assign refractory[k] = (refractory_state[k] > 0);
   end
endgenerate

endmodule
