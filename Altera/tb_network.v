
module tb_network
   #(parameter 
      USB_DEPTH_BITS=4,
      USB_WIDTH_BITS=8,
      FIFO_USB_WIDTH_BITS=16,
      GLOBAL_CELL_BITS=8,
      CELLS=16,
      CELL_BITS=4,
      SYNAPSE_BITS=5,
      DENDRITE_BITS=5,
      FIFO_LOCAL_WIDTH_BITS=CELL_BITS+DENDRITE_BITS,
      FIFO_DISTANT_WIDTH_BITS=16,
      FIFO_DEPTH_BITS=3,
      VMEM_BITS=12,
      WEIGHT_BITS=VMEM_BITS,
      WEIGHT_RAM_DEPTH_BITS=CELL_BITS+DENDRITE_BITS,
      WEIGHT_RAM_WIDTH_BITS=WEIGHT_BITS,SPIKE_TIME_RAM_DEPTH_BITS=CELL_BITS,
      SPIKE_TIME_RAM_WIDTH_BITS=4,
      PSP_RAM_DEPTH_BITS=CELL_BITS,
      PSP_RAM_WIDTH_BITS=VMEM_BITS,
      RAM_DEPTH_BITS=CELL_BITS,
      RAM_WIDTH_BITS=VMEM_BITS)
   (
      input rst,
      input clk,
      
      input synapseType,
      input processCell,
   
      input [USB_WIDTH_BITS-1:0]  usb_data,
      input                       usb_req,
      output                      usb_ack,
      output [USB_WIDTH_BITS-1:0] usbout_data,
      output                      usbout_req,
      input                       usbout_ack
   
   );

///////////////////
// NEURON CORES
wire [15:0] dendDistantDownFIFO_full, dendDistantDownFIFO_wrreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] dendDistantDownFIFO_din [15:0];

wire [15:0] dendDistantUpFIFO_empty, dendDistantUpFIFO_rdreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] dendDistantUpFIFO_dout [15:0];

genvar i;
generate 
   for(i=0; i<16; i=i+1)begin : gen_nc
      neuron_core 
         #(i,
            GLOBAL_CELL_BITS, CELLS, CELL_BITS, SYNAPSE_BITS, DENDRITE_BITS, FIFO_DEPTH_BITS, FIFO_LOCAL_WIDTH_BITS, FIFO_DISTANT_WIDTH_BITS,
            VMEM_BITS, WEIGHT_BITS, WEIGHT_RAM_DEPTH_BITS, WEIGHT_RAM_WIDTH_BITS, SPIKE_TIME_RAM_DEPTH_BITS, SPIKE_TIME_RAM_WIDTH_BITS,
            PSP_RAM_DEPTH_BITS, PSP_RAM_WIDTH_BITS, RAM_DEPTH_BITS, RAM_WIDTH_BITS)
      NEURON_CORE_i
         (rst, clk, processCell, synapseType,
          dendDistantDownFIFO_full[i], dendDistantDownFIFO_din[i], dendDistantDownFIFO_wrreq[i],
          dendDistantUpFIFO_empty[i], dendDistantUpFIFO_rdreq[i], dendDistantUpFIFO_dout[i]);
   end
endgenerate

//////////////////////
// ARBITERS: LAYER 1
wire [7:0] L1_inDown_full, L1_inDown_wrreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] L1_inDown_din [7:0];
wire [7:0] L1_outUp_empty, L1_outUp_rdreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] L1_outUp_dout [7:0];

genvar j;
generate
   for(j=0; j<8; j=j+1)begin : gen_l1
      arbiter #(GLOBAL_CELL_BITS+DENDRITE_BITS-1, GLOBAL_CELL_BITS+DENDRITE_BITS-3, j, 
                GLOBAL_CELL_BITS, DENDRITE_BITS, FIFO_DEPTH_BITS, FIFO_DISTANT_WIDTH_BITS)
      ARBITER_L1_j 
         (rst, clk, 
         L1_inDown_full[j],               L1_inDown_din[j],               L1_inDown_wrreq[j],
         dendDistantUpFIFO_empty[2*j],    dendDistantUpFIFO_rdreq[2*j],   dendDistantUpFIFO_dout[2*j],
         dendDistantUpFIFO_empty[2*j+1],  dendDistantUpFIFO_rdreq[2*j+1], dendDistantUpFIFO_dout[2*j+1],
         L1_outUp_empty[j],               L1_outUp_rdreq[j],              L1_outUp_dout[j],
         dendDistantDownFIFO_full[2*j],   dendDistantDownFIFO_din[2*j],   dendDistantDownFIFO_wrreq[2*j],
         dendDistantDownFIFO_full[2*j+1], dendDistantDownFIFO_din[2*j+1], dendDistantDownFIFO_wrreq[2*j+1]);
   end
endgenerate

//////////////////////
// ARBITERS: LAYER 2
wire [3:0] L2_inDown_full, L2_inDown_wrreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] L2_inDown_din [3:0];
wire [3:0] L2_outUp_empty, L2_outUp_rdreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] L2_outUp_dout [3:0];

genvar k;
generate
   for(k=0; k<4; k=k+1)begin : gen_l2
      arbiter #(GLOBAL_CELL_BITS+DENDRITE_BITS-1, GLOBAL_CELL_BITS+DENDRITE_BITS-2, k, 
                GLOBAL_CELL_BITS, DENDRITE_BITS, FIFO_DEPTH_BITS, FIFO_DISTANT_WIDTH_BITS)
      ARBITER_L2_k 
         (rst, clk, 
         L2_inDown_full[k],     L2_inDown_din[k],      L2_inDown_wrreq[k],
         L1_outUp_empty[2*k],   L1_outUp_rdreq[2*k],   L1_outUp_dout[2*k],
         L1_outUp_empty[2*k+1], L1_outUp_rdreq[2*k+1], L1_outUp_dout[2*k+1],
         L2_outUp_empty[k],     L2_outUp_rdreq[k],     L2_outUp_dout[k],
         L1_inDown_full[2*k],   L1_inDown_din[2*k],    L1_inDown_wrreq[2*k],
         L1_inDown_full[2*k+1], L1_inDown_din[2*k+1],  L1_inDown_wrreq[2*k+1]);
   end
endgenerate

//////////////////////
// ARBITERS: LAYER 3
wire [1:0] L3_inDown_full, L3_inDown_wrreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] L3_inDown_din [1:0];
wire [1:0] L3_outUp_empty, L3_outUp_rdreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] L3_outUp_dout [1:0];

genvar m;
generate
   for(m=0; m<2; m=m+1)begin : gen_l3
      parameter L3_ADDRESS_FILTER_m = m;
      arbiter #(GLOBAL_CELL_BITS+DENDRITE_BITS-1, GLOBAL_CELL_BITS+DENDRITE_BITS-1, L3_ADDRESS_FILTER_m, 
                GLOBAL_CELL_BITS, DENDRITE_BITS, FIFO_DEPTH_BITS, FIFO_DISTANT_WIDTH_BITS)
      ARBITER_L3_m 
         (rst, clk, 
         L3_inDown_full[m],     L3_inDown_din[m],      L3_inDown_wrreq[m],
         L2_outUp_empty[2*m],   L2_outUp_rdreq[2*m],   L2_outUp_dout[2*m],
         L2_outUp_empty[2*m+1], L2_outUp_rdreq[2*m+1], L2_outUp_dout[2*m+1],
         L3_outUp_empty[m],     L3_outUp_rdreq[m],     L3_outUp_dout[m],
         L2_inDown_full[2*m],   L2_inDown_din[2*m],    L2_inDown_wrreq[2*m],
         L2_inDown_full[2*m+1], L2_inDown_din[2*m+1],  L2_inDown_wrreq[2*m+1]);
   end
endgenerate

//////////////////////
// ARBITERS: LAYER 4
wire L4_inDown_full, L4_inDown_wrreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] L4_inDown_din;
wire L4_outUp_empty, L4_outUp_rdreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] L4_outUp_dout;

wire inUsbFIFO_empty, inUsbFIFO_rdreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] inUsbFIFO_dout;

wire outUsbFIFO_full, outUsbFIFO_wrreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] outUsbFIFO_din;

arbiter_top #(GLOBAL_CELL_BITS, DENDRITE_BITS, FIFO_DEPTH_BITS, FIFO_DISTANT_WIDTH_BITS)
      ARBITER_L4
         (rst, clk,
          inUsbFIFO_empty, inUsbFIFO_rdreq, inUsbFIFO_dout,
          L3_outUp_empty[0], L3_outUp_rdreq[0], L3_outUp_dout[0],
          L3_outUp_empty[1], L3_outUp_rdreq[1], L3_outUp_dout[1],
          outUsbFIFO_full, outUsbFIFO_din, outUsbFIFO_wrreq,
          L3_inDown_full[0], L3_inDown_din[0],  L3_inDown_wrreq[0],
          L3_inDown_full[1], L3_inDown_din[1],  L3_inDown_wrreq[1]);

///////////////////
// USB CONTROLLER 
controller_usb #(USB_DEPTH_BITS, USB_WIDTH_BITS, FIFO_USB_WIDTH_BITS)
   USB_CTRL (rst, clk, 
             usb_data, usb_req, usb_ack, inUsbFIFO_empty, inUsbFIFO_rdreq, inUsbFIFO_dout,  
             outUsbFIFO_full, outUsbFIFO_din, outUsbFIFO_wrreq, usbout_data, usbout_req, usbout_ack);
      
endmodule
