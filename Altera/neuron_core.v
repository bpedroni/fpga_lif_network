
module neuron_core 
   #(parameter 
      ADDRESS_FILTER=1'b0,
      GLOBAL_CELL_BITS=6,
      CELLS=16,
      CELL_BITS=4, 
      SYNAPSE_BITS=2,
      DENDRITE_BITS=2,
      FIFO_DEPTH_BITS=4,
      FIFO_LOCAL_WIDTH_BITS=CELL_BITS+DENDRITE_BITS,
      FIFO_DISTANT_WIDTH_BITS=24,
      VMEM_BITS=12,
      WEIGHT_BITS=VMEM_BITS,
      WEIGHT_RAM_DEPTH_BITS=CELL_BITS+DENDRITE_BITS,
      WEIGHT_RAM_WIDTH_BITS=WEIGHT_BITS,
      SPIKE_TIME_RAM_DEPTH_BITS=CELL_BITS,
      SPIKE_TIME_RAM_WIDTH_BITS=5,
      PSP_RAM_DEPTH_BITS=CELL_BITS,
      PSP_RAM_WIDTH_BITS=VMEM_BITS,
      RAM_DEPTH_BITS=CELL_BITS,
      RAM_WIDTH_BITS=VMEM_BITS)
   
   (
      input rst,
      input clk,
      
      input processCell, 
      input synapseType,


      ///////////////////////
      // DISTANT DOWN FIFO //
      ///////////////////////      
      
      output                              dendDistantDownFIFO_full,
      input [FIFO_DISTANT_WIDTH_BITS-1:0] dendDistantDownFIFO_din,
      input                               dendDistantDownFIFO_wrreq,

      
      /////////////////////
      // DISTANT UP FIFO //
      /////////////////////      
      
      output                               dendDistantUpFIFO_empty,
      input                                dendDistantUpFIFO_rdreq,
      output [FIFO_DISTANT_WIDTH_BITS-1:0] dendDistantUpFIFO_dout
   
   );


// DISTANT DOWN DENDRITE EVENT FIFO
wire dendDistantDownFIFO_empty, dendDistantDownFIFO_rdreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] dendDistantDownFIFO_dout;

// DISTANT UP DENDRITE EVENT FIFO
wire dendDistantUpFIFO_full, dendDistantUpFIFO_wrreq;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] dendDistantUpFIFO_din;
      
// LOCAL DENTRITE EVENT FIFO
wire dendLocalFIFO_empty, dendLocalFIFO_rdreq, dendLocalFIFO_full, dendLocalFIFO_wrreq;
wire [FIFO_LOCAL_WIDTH_BITS-1:0] dendLocalFIFO_dout, dendLocalFIFO_din;

// CELL PARAMETER CONFIGURATION FIFO 
wire configParamFIFO_full, configParamFIFO_wrreq, configParamFIFO_empty, configParamFIFO_rdreq;
wire [15:0] configParamFIFO_din;
wire [31:0] configParamFIFO_dout;

// DENDRITE EVENT FIFO
wire dendEventFIFO_full, dendEventFIFO_wrreq, dendEventFIFO_empty, dendEventFIFO_rdreq;
wire [FIFO_LOCAL_WIDTH_BITS-1:0] dendEventFIFO_din, dendEventFIFO_dout;

// NEURON REFRACTORY
wire [CELLS-1:0] refractory;

// PSP RAM //
wire [CELL_BITS-1:0] cellPspRAM_wradd, cellPspRAM_rdadd;
wire [VMEM_BITS-1:0] cellPspRAM_din, cellPspRAM_dout;
wire cellPspRAM_wren;

// OUTGOING SPIKE EVENT
wire spikeEventFIFO_full, spikeEventFIFO_wrreq, spikeEventFIFO_empty, spikeEventFIFO_rdreq;
wire [CELL_BITS-1:0] spikeEventFIFO_din, spikeEventFIFO_dout;


ip_dcfifo #(FIFO_DEPTH_BITS, FIFO_DISTANT_WIDTH_BITS)
   FIFO_DIST_DOWN_EVENT (clk, dendDistantDownFIFO_din, dendDistantDownFIFO_rdreq, rst, dendDistantDownFIFO_wrreq, 
                         dendDistantDownFIFO_empty, dendDistantDownFIFO_full, dendDistantDownFIFO_dout);

router_incoming #(CELL_BITS, DENDRITE_BITS, FIFO_LOCAL_WIDTH_BITS, FIFO_DISTANT_WIDTH_BITS)
   ROUTER_INCOMING 
      (rst, clk, synapseType,
       dendDistantDownFIFO_empty, dendDistantDownFIFO_rdreq, dendDistantDownFIFO_dout,
       dendLocalFIFO_empty, dendLocalFIFO_rdreq, dendLocalFIFO_dout,
       configParamFIFO_full, configParamFIFO_din, configParamFIFO_wrreq,
       dendEventFIFO_full, dendEventFIFO_din, dendEventFIFO_wrreq);

ip_dcfifo #(FIFO_DEPTH_BITS, FIFO_LOCAL_WIDTH_BITS)
   FIFO_DEND_EVENT (clk, dendEventFIFO_din, dendEventFIFO_rdreq, rst, dendEventFIFO_wrreq, 
                    dendEventFIFO_empty, dendEventFIFO_full, dendEventFIFO_dout);

synapse #(CELLS, CELL_BITS, SYNAPSE_BITS, DENDRITE_BITS, VMEM_BITS, FIFO_LOCAL_WIDTH_BITS, WEIGHT_BITS,
          WEIGHT_RAM_DEPTH_BITS, WEIGHT_RAM_WIDTH_BITS, SPIKE_TIME_RAM_DEPTH_BITS, SPIKE_TIME_RAM_WIDTH_BITS)
   SYNAPSE
      (rst, clk, processCell, synapseType, 
       dendEventFIFO_empty, dendEventFIFO_rdreq, dendEventFIFO_dout,
       refractory, cellPspRAM_wradd, cellPspRAM_din, cellPspRAM_wren);

ip_ddr #(PSP_RAM_DEPTH_BITS, PSP_RAM_WIDTH_BITS) 
   PSP_RAM (rst, clk, cellPspRAM_din, cellPspRAM_rdadd, cellPspRAM_wradd, cellPspRAM_wren, cellPspRAM_dout);

neuron_processor #(CELLS, CELL_BITS, VMEM_BITS, RAM_DEPTH_BITS, RAM_WIDTH_BITS)
   NEURON_PROCESSOR 
      (rst, clk, processCell, refractory,
       configParamFIFO_empty, configParamFIFO_rdreq, configParamFIFO_dout,
       cellPspRAM_rdadd, cellPspRAM_dout,
       spikeEventFIFO_full, spikeEventFIFO_din, spikeEventFIFO_wrreq);

ip_dcfifo_mixed_widths #(FIFO_DEPTH_BITS, FIFO_DISTANT_WIDTH_BITS, 2*FIFO_DISTANT_WIDTH_BITS)
   FIFO_CONFIG_CELL_PARAM (rst, configParamFIFO_din, clk, configParamFIFO_rdreq, clk, configParamFIFO_wrreq, 
                           configParamFIFO_dout, configParamFIFO_empty, configParamFIFO_full);

ip_dcfifo #(FIFO_DEPTH_BITS, CELL_BITS)
   FIFO_SPIKE_EVENT (clk, spikeEventFIFO_din, spikeEventFIFO_rdreq, rst, spikeEventFIFO_wrreq, 
                     spikeEventFIFO_empty, spikeEventFIFO_full, spikeEventFIFO_dout);

router_outgoing #(ADDRESS_FILTER, GLOBAL_CELL_BITS, CELL_BITS, SYNAPSE_BITS, DENDRITE_BITS, FIFO_DISTANT_WIDTH_BITS)
   ROUTER_OUTGOING
      (rst, clk, 
       spikeEventFIFO_empty, spikeEventFIFO_rdreq, spikeEventFIFO_dout,
       dendLocalFIFO_full, dendLocalFIFO_din, dendLocalFIFO_wrreq,
       dendDistantUpFIFO_full, dendDistantUpFIFO_din, dendDistantUpFIFO_wrreq);

ip_dcfifo #(FIFO_DEPTH_BITS, CELL_BITS+DENDRITE_BITS)
   FIFO_LOCAL_EVENT (clk, dendLocalFIFO_din, dendLocalFIFO_rdreq, rst, dendLocalFIFO_wrreq, 
                     dendLocalFIFO_empty, dendLocalFIFO_full, dendLocalFIFO_dout);

ip_dcfifo #(FIFO_DEPTH_BITS, FIFO_DISTANT_WIDTH_BITS)
   FIFO_DIST_UP_EVENT (clk, dendDistantUpFIFO_din, dendDistantUpFIFO_rdreq, rst, dendDistantUpFIFO_wrreq, 
                       dendDistantUpFIFO_empty, dendDistantUpFIFO_full, dendDistantUpFIFO_dout);
                     
endmodule
   