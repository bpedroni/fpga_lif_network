
module router_outgoing
   #(parameter 
      ADDRESS_FILTER,
      GLOBAL_CELL_BITS=10,
      CELL_BITS=6,
      SYNAPSE_BITS=4,
      DENDRITE_BITS=4,
      FIFO_DISTANT_WIDTH_BITS=16)
   (
      input rst,
      input clk,
   
      //////////////////////////
      // INCOMING SPIKE EVENT //
      //////////////////////////
      
      input                 spikeEventFIFO_empty,
      output reg            spikeEventFIFO_rdreq,
      input [CELL_BITS-1:0] spikeEventFIFO_dout,
      
      
      //////////////////////////
      // LOCAL DENDRITE EVENT //
      //////////////////////////
      
      input                                dendLocalFIFO_full,
      output [CELL_BITS+DENDRITE_BITS-1:0] dendLocalFIFO_din,
      output reg                           dendLocalFIFO_wrreq,

      
      ////////////////////////////
      // DISTANT DENDRITE EVENT //
      ////////////////////////////
      
      input                                dendDistantUpFIFO_full,
      output [FIFO_DISTANT_WIDTH_BITS-1:0] dendDistantUpFIFO_din,
      output reg                           dendDistantUpFIFO_wrreq
   );


// synapse defines the second part of the routing table RAM address
reg [SYNAPSE_BITS-1:0] synapse;
reg move_to_next_synapse;


//////////////////////////////////////////////////////////////////////////
// RAM read address defined by current neuron cell and synapses addresses
wire [CELL_BITS+SYNAPSE_BITS-1:0] routingRAM_rdadd = {spikeEventFIFO_dout, synapse};
wire [GLOBAL_CELL_BITS+DENDRITE_BITS-1:0] routingRAM_dout;

// RAM will be written using memory initialization file (MIF)
wire                                      routingRAM_wren  = 0;
wire [CELL_BITS+SYNAPSE_BITS-1:0]         routingRAM_wradd = 0;
wire [GLOBAL_CELL_BITS+DENDRITE_BITS-1:0] routingRAM_din   = 0;

ip_ddr_route #(CELL_BITS+SYNAPSE_BITS, GLOBAL_CELL_BITS+DENDRITE_BITS) ROUTING_RAM
   (rst, clk, routingRAM_din, routingRAM_rdadd, routingRAM_wradd, routingRAM_wren, routingRAM_dout);

   
////////////////////////////////////////////////////////////////
// address_filter defines if is local or distant dendrite event
wire [GLOBAL_CELL_BITS-CELL_BITS-1:0] address_filter = routingRAM_dout[GLOBAL_CELL_BITS+DENDRITE_BITS-1:CELL_BITS+DENDRITE_BITS];

// local events (side)
assign dendLocalFIFO_din = routingRAM_dout[CELL_BITS+DENDRITE_BITS-1:0];

// distant events (up)
reg send_spike_out;
wire [FIFO_DISTANT_WIDTH_BITS-1:0] spikeOut;
assign spikeOut[FIFO_DISTANT_WIDTH_BITS-1] = 1'b1;
assign spikeOut[FIFO_DISTANT_WIDTH_BITS-2:CELL_BITS] = 0;
assign spikeOut[CELL_BITS-1:0] = spikeEventFIFO_dout;
assign dendDistantUpFIFO_din = send_spike_out ? spikeOut : routingRAM_dout;


///////////////////////////////////////////
// state machine: servicing dendrite event
parameter [3:0] STATE_RESET             = 4'd0;
parameter [3:0] STATE_IDLE              = 4'd1;
parameter [3:0] STATE_SEND_SPIKE_OUT    = 4'd2;
parameter [3:0] STATE_READ_ROUTING_RAM1 = 4'd3;
parameter [3:0] STATE_READ_ROUTING_RAM2 = 4'd4;
parameter [3:0] STATE_READ_ROUTING_RAM3 = 4'd5;
parameter [3:0] STATE_ROUTE_LOCAL       = 4'd6;
parameter [3:0] STATE_ROUTE_DISTANT     = 4'd7;
parameter [3:0] STATE_NEXT_SYNAPSE      = 4'd8;

reg [3:0] curr_state;
reg [3:0] next_state;

always @(posedge rst or posedge clk)
begin
   if(rst)
      curr_state <= STATE_RESET;
   else
      curr_state <= next_state;
end

always @(*)
begin
   spikeEventFIFO_rdreq     = 0;
   send_spike_out          = 0;
   dendLocalFIFO_wrreq     = 0;
   dendDistantUpFIFO_wrreq = 0;
   move_to_next_synapse    = 0;

   next_state = curr_state;

   case(curr_state)
      STATE_RESET:
      begin
         next_state = STATE_IDLE;
      end
      
      // wait for incoming spike event
      STATE_IDLE:
      begin
         if(~spikeEventFIFO_empty)begin
            spikeEventFIFO_rdreq = 1;
            next_state = STATE_SEND_SPIKE_OUT;
         end
      end

      // send spike out to USB
      STATE_SEND_SPIKE_OUT:
      begin
         send_spike_out = 1;
         if(~dendDistantUpFIFO_full)begin
            dendDistantUpFIFO_wrreq = 1;
            next_state = STATE_READ_ROUTING_RAM1;
         end
      end
            
      // two clock cycles to read routing table RAM
      STATE_READ_ROUTING_RAM1:
      begin
         next_state = STATE_READ_ROUTING_RAM2;
      end
      STATE_READ_ROUTING_RAM2:
      begin
         next_state = STATE_READ_ROUTING_RAM3;
      end
      
      // verify if is local or distant dendrite event
      STATE_READ_ROUTING_RAM3:
      begin
         if(routingRAM_dout != 0)begin
            if(address_filter == ADDRESS_FILTER)
               next_state = STATE_ROUTE_LOCAL;
            else
               next_state = STATE_ROUTE_DISTANT;
         end else
            next_state = STATE_IDLE;
      end
      
      // write to local dendrite event FIFO and move on to next synapse
      STATE_ROUTE_LOCAL:
      begin
         if(~dendLocalFIFO_full)begin
            dendLocalFIFO_wrreq = 1;
            next_state = STATE_NEXT_SYNAPSE;
         end
      end
      
      // write to distant dendrite event FIFO and move on to next synapse
      STATE_ROUTE_DISTANT:
      begin
         if(~dendDistantUpFIFO_full)begin
            dendDistantUpFIFO_wrreq = 1;
            next_state = STATE_NEXT_SYNAPSE;
         end
      end
      
      // verify if has not reached synapse limit
      STATE_NEXT_SYNAPSE:
      begin
         if(synapse < ((1<<SYNAPSE_BITS)-1))begin
            move_to_next_synapse = 1;
            next_state = STATE_READ_ROUTING_RAM1;
         end else begin
            move_to_next_synapse = 0;
            next_state = STATE_IDLE;
         end
      end
      
      default:
      begin
         spikeEventFIFO_rdreq     = 0;
         send_spike_out          = 0;
         dendLocalFIFO_wrreq     = 0;
         dendDistantUpFIFO_wrreq = 0;
         move_to_next_synapse    = 0;
         next_state = STATE_RESET;
      end
   endcase
end

// cycle through synapses; hault while servicing
always @(posedge rst or posedge clk)
begin
   if(rst)
      synapse <= 0;
   else
   begin
      if(curr_state == STATE_IDLE)
         synapse <= 0;
      else if(move_to_next_synapse)
         synapse <= synapse + 1;
   end
end

endmodule
