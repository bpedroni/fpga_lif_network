
module router_incoming 
   #(parameter 
      CELL_BITS=4, 
      DENDRITE_BITS=4,
      FIFO_LOCAL_WIDTH_BITS=CELL_BITS+DENDRITE_BITS,
      FIFO_DISTANT_WIDTH_BITS=16)
   (
      input rst,
      input clk,
      
      input synapseType,
      
      /////////////////////////////////
      // DISTANT DENTRITE EVENT FIFO //
      /////////////////////////////////
      
      input                               dendDistantDownFIFO_empty,
      output reg                          dendDistantDownFIFO_rdreq,
      input [FIFO_DISTANT_WIDTH_BITS-1:0] dendDistantDownFIFO_dout,
      
      
      ///////////////////////////////
      // LOCAL DENTRITE EVENT FIFO //
      ///////////////////////////////
      
      input                             dendLocalFIFO_empty,
      output reg                        dendLocalFIFO_rdreq,
      input [FIFO_LOCAL_WIDTH_BITS-1:0] dendLocalFIFO_dout,
      

      ///////////////////////////
      // NEURON PARAMETERS RAM //
      ///////////////////////////
      
      input                                configParamFIFO_full,
      output [FIFO_DISTANT_WIDTH_BITS-1:0] configParamFIFO_din,
      output reg                           configParamFIFO_wrreq,
    
  
      ///////////////////////////
      // DENDRITE EVENT OUTPUT //
      ///////////////////////////
      
      input                              dendEventFIFO_full,
      output [FIFO_LOCAL_WIDTH_BITS-1:0] dendEventFIFO_din,
      output reg                         dendEventFIFO_wrreq
      
   );


// Neuron parameter configuration
assign configParamFIFO_din = dendDistantDownFIFO_dout;

// Dendrite event processing
reg distant_event;
assign dendEventFIFO_din = distant_event ? dendDistantDownFIFO_dout[CELL_BITS+DENDRITE_BITS-1:0] : dendLocalFIFO_dout;

// state machine: servicing dendrite event
parameter [2:0] STATE_RESET                       = 3'd0;
parameter [2:0] STATE_IDLE                        = 3'd1;
parameter [2:0] STATE_DISTANT_EVENT               = 3'd2;
parameter [2:0] STATE_CONFIGURE_NEURON_PARAMETER1 = 3'd3;
parameter [2:0] STATE_READ_NEXT_CONFIG_WORD       = 3'd4;
parameter [2:0] STATE_CONFIGURE_NEURON_PARAMETER2 = 3'd5;
parameter [2:0] STATE_DENDRITE_EVENT              = 3'd6;

reg [2:0] curr_state;
reg [2:0] next_state;

always @(posedge rst or posedge clk)
begin
   if(rst)
      curr_state <= STATE_RESET;
   else
      curr_state <= next_state;
end

always @(*)
begin

   dendDistantDownFIFO_rdreq = 0;
   dendLocalFIFO_rdreq       = 0;
   configParamFIFO_wrreq     = 0;
   dendEventFIFO_wrreq       = 0;
   
   next_state = curr_state;

   case(curr_state)
      STATE_RESET:
      begin
         next_state = STATE_IDLE;
      end
      
      // wait for incoming spike event
      STATE_IDLE:
      begin
         if(~dendDistantDownFIFO_empty)begin
            dendDistantDownFIFO_rdreq = 1;
            next_state = STATE_DISTANT_EVENT;
         end else if(~dendLocalFIFO_empty)begin
            dendLocalFIFO_rdreq = 1;
            next_state = STATE_DENDRITE_EVENT;
         end
      end
      
      // verify if is configuration or dendrite event
      STATE_DISTANT_EVENT:
      begin
         if(dendDistantDownFIFO_dout[FIFO_DISTANT_WIDTH_BITS-1])
            next_state = STATE_CONFIGURE_NEURON_PARAMETER1;
         else
            next_state = STATE_DENDRITE_EVENT;
      end
      
      ////////////////////////////////////
      STATE_CONFIGURE_NEURON_PARAMETER1:
      begin
         if(~configParamFIFO_full)begin
            configParamFIFO_wrreq = 1;
            next_state = STATE_READ_NEXT_CONFIG_WORD;
         end
      end
      STATE_READ_NEXT_CONFIG_WORD:
      begin
         if(~dendDistantDownFIFO_empty)begin
            dendDistantDownFIFO_rdreq = 1;
            next_state = STATE_CONFIGURE_NEURON_PARAMETER2;
         end
      end
      STATE_CONFIGURE_NEURON_PARAMETER2:
      begin
         if(~configParamFIFO_full)begin
            configParamFIFO_wrreq = 1;
            next_state = STATE_IDLE;
         end
      end
      
      ////////////////////////////////////
      STATE_DENDRITE_EVENT:
      begin
         if(~dendEventFIFO_full)begin
            dendEventFIFO_wrreq = 1;
            next_state = STATE_IDLE;
         end
      end
      
      default:
      begin
         dendDistantDownFIFO_rdreq = 0;
         dendLocalFIFO_rdreq       = 0;
         configParamFIFO_wrreq     = 0;
         dendEventFIFO_wrreq       = 0;
         next_state = STATE_RESET;
      end
   endcase
end

// control MUX for distant/local event
always @(posedge rst or posedge clk)
begin
   if(rst)
      distant_event <= 0;
   else if(curr_state == STATE_DISTANT_EVENT)
      distant_event <= 1;
   else if(curr_state == STATE_IDLE)
      distant_event <= 0;
end

endmodule

