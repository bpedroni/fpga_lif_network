module ip_ddr_route #(parameter DEPTH_BITS=5, WIDTH_BITS=8) (
	aclr,
	clock,
	data,
	rdaddress,
	wraddress,
	wren,
	q);

	input	  aclr;
	input	  clock;
	input	[WIDTH_BITS-1:0]  data;
	input	[DEPTH_BITS-1:0]  rdaddress;
	input	[DEPTH_BITS-1:0]  wraddress;
	input	  wren;
	output	[WIDTH_BITS-1:0]  q;

	tri0	  aclr;
	tri1	  clock;
	tri0	  wren;

	wire [WIDTH_BITS-1:0] sub_wire0;
	wire [WIDTH_BITS-1:0] q = sub_wire0[WIDTH_BITS-1:0];

	altsyncram	altsyncram_component (
				.aclr0 (aclr),
				.address_a (wraddress),
				.address_b (rdaddress),
				.clock0 (clock),
				.data_a (data),
				.wren_a (wren),
				.q_b (sub_wire0),
				.aclr1 (1'b0),
				.addressstall_a (1'b0),
				.addressstall_b (1'b0),
				.byteena_a (1'b1),
				.byteena_b (1'b1),
				.clock1 (1'b1),
				.clocken0 (1'b1),
				.clocken1 (1'b1),
				.clocken2 (1'b1),
				.clocken3 (1'b1),
				.data_b ({WIDTH_BITS{1'b1}}),
				.eccstatus (),
				.q_a (),
				.rden_a (1'b1),
				.rden_b (1'b1),
				.wren_b (1'b0));
	defparam
		altsyncram_component.address_aclr_b = "CLEAR0",
		altsyncram_component.address_reg_b = "CLOCK0",
		altsyncram_component.clock_enable_input_a = "BYPASS",
		altsyncram_component.clock_enable_input_b = "BYPASS",
		altsyncram_component.clock_enable_output_b = "BYPASS",
		altsyncram_component.init_file = "mif_route.mif",
		altsyncram_component.intended_device_family = "Cyclone IV E",
		altsyncram_component.lpm_type = "altsyncram",
		altsyncram_component.numwords_a = 1 << DEPTH_BITS,
		altsyncram_component.numwords_b = 1 << DEPTH_BITS,
		altsyncram_component.operation_mode = "DUAL_PORT",
		altsyncram_component.outdata_aclr_b = "CLEAR0",
		altsyncram_component.outdata_reg_b = "CLOCK0",
		altsyncram_component.power_up_uninitialized = "FALSE",
		altsyncram_component.read_during_write_mode_mixed_ports = "DONT_CARE",
		altsyncram_component.widthad_a = DEPTH_BITS,
		altsyncram_component.widthad_b = DEPTH_BITS,
		altsyncram_component.width_a = WIDTH_BITS,
		altsyncram_component.width_b = WIDTH_BITS,
		altsyncram_component.width_byteena_a = 1;

endmodule
