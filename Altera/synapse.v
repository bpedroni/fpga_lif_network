
module synapse 
   #(parameter 
      CELLS=64,
      CELL_BITS=6, 
      SYNAPSE_BITS=4,
      DENDRITE_BITS=4,
      VMEM_BITS=12,
      FIFO_WIDTH_BITS=CELL_BITS+DENDRITE_BITS,
      WEIGHT_BITS=VMEM_BITS,
      WEIGHT_RAM_DEPTH_BITS=CELL_BITS+DENDRITE_BITS,
      WEIGHT_RAM_WIDTH_BITS=WEIGHT_BITS,
      SPIKE_TIME_RAM_DEPTH_BITS=CELL_BITS,
      SPIKE_TIME_RAM_WIDTH_BITS=5)
   (
      input rst,
      input clk,
      
      input processCell,
      input synapseType,
      
      
      /////////////////////////
      // DENTRITE EVENT FIFO //
      /////////////////////////
      
      input                       dendEventFIFO_empty,
      output reg                  dendEventFIFO_rdreq,
      input [FIFO_WIDTH_BITS-1:0] dendEventFIFO_dout,
      
      
      ///////////////////////
      // NEURON REFRACTORY //
      ///////////////////////
      
      input [CELLS-1:0] refractory,

      
      /////////////
      // PSP RAM //
      /////////////
      
      output [CELL_BITS-1:0] cellPspRAM_wradd,
      output [VMEM_BITS-1:0] cellPspRAM_din,
      output reg             cellPspRAM_wren

   );
   
   
// Process dendrite event / pre-PSP->PSP transfer
reg                  move_to_next_cell;
reg [CELL_BITS-1:0]  cell_being_processed;
reg                  select_transfer;           // used to indicate if processing dendrite event (0) or pre-PSP->PSP transfer (1)
wire [CELL_BITS-1:0] dendEvent_cell = dendEventFIFO_dout[CELL_BITS+DENDRITE_BITS-1:DENDRITE_BITS];
   
// RAMs
wire [WEIGHT_RAM_WIDTH_BITS-1:0] weightRAM_din, weightRAM_dout;
wire [WEIGHT_RAM_DEPTH_BITS-1:0] weightRAM_wradd, weightRAM_rdadd;
wire weightRAM_wren = 1'b0;

//wire [SPIKE_TIME_RAM_WIDTH_BITS-1:0] spikeTimeRAM_din, spikeTimeRAM_dout;
//wire [SPIKE_TIME_RAM_DEPTH_BITS-1:0] spikeTimeRAM_wradd, spikeTimeRAM_rdadd;
//reg spikeTimeRAM_wren;

wire [VMEM_BITS-1:0] precellPspRAM_din, precellPspRAM_dout;
wire [CELL_BITS-1:0] precellPspRAM_wradd, precellPspRAM_rdadd;
reg precellPspRAM_wren;

// RAM declarations
ip_ddr_weights #(WEIGHT_RAM_DEPTH_BITS, WEIGHT_RAM_WIDTH_BITS) WEIGHT_RAM
   (rst, clk, weightRAM_din, weightRAM_rdadd, weightRAM_wradd, weightRAM_wren, weightRAM_dout);
//ip_ddr #(SPIKE_TIME_RAM_DEPTH_BITS, SPIKE_TIME_RAM_WIDTH_BITS) SPIKE_TIME_RAM
//   (rst, clk, spikeTimeRAM_din, spikeTimeRAM_rdadd, spikeTimeRAM_wradd, spikeTimeRAM_wren, spikeTimeRAM_dout);
ip_ddr #(CELL_BITS, VMEM_BITS) PRE_PSP_RAM
   (rst, clk, precellPspRAM_din, precellPspRAM_rdadd, precellPspRAM_wradd, precellPspRAM_wren, precellPspRAM_dout);

   
/////////////////////////////////////////////////////////////
// state machine: servicing global 'tick' and dendrite event
parameter [3:0] STATE_RESET                    = 4'd0;
parameter [3:0] STATE_IDLE                     = 4'd1;
parameter [3:0] STATE_TRANSFER_PRE_PSP1        = 4'd2;
parameter [3:0] STATE_TRANSFER_PRE_PSP2        = 4'd3;
parameter [3:0] STATE_VERIFY_SYNAPSE_TYPE      = 4'd4;
parameter [3:0] STATE_WAIT_PRE_PSP_RAM1        = 4'd5;
parameter [3:0] STATE_WAIT_PRE_PSP_RAM2        = 4'd6;
parameter [3:0] STATE_VERIFY_REFRACTORY        = 4'd7;
parameter [3:0] STATE_READ_PRE_PSP_AND_WEIGHT1 = 4'd8;
parameter [3:0] STATE_READ_PRE_PSP_AND_WEIGHT2 = 4'd9;

reg [3:0] curr_state;
reg [3:0] next_state;

always @(posedge rst or posedge clk)
begin
   if(rst)
      curr_state <= STATE_RESET;
   else
      curr_state <= next_state;
end

always @(*)
begin

   select_transfer     = 0;
   dendEventFIFO_rdreq = 0;
   cellPspRAM_wren         = 0;
   move_to_next_cell   = 0;
   precellPspRAM_wren      = 0;
   
   next_state = curr_state;

   case(curr_state)
      STATE_RESET:
      begin
         next_state = STATE_IDLE;
      end
      
      STATE_IDLE:
      begin
         // global clock 'tick': transfer pre-PSP->PSP
         if(processCell)begin
            select_transfer = 1;
            next_state = STATE_TRANSFER_PRE_PSP1;

         // process dendrite event
         end else if(~dendEventFIFO_empty)begin
            select_transfer = 0;
            dendEventFIFO_rdreq = 1;
            next_state = STATE_VERIFY_REFRACTORY;
         end
      end
      
      //////////////////////////////////////////
      // two clock cycles to read pre-PSP RAM
      STATE_TRANSFER_PRE_PSP1:
      begin
         select_transfer = 1;
         next_state <= STATE_TRANSFER_PRE_PSP2;
      end
      STATE_TRANSFER_PRE_PSP2:
      begin
         select_transfer = 1;
         cellPspRAM_wren = 1;
         next_state = STATE_VERIFY_SYNAPSE_TYPE;
      end
      STATE_VERIFY_SYNAPSE_TYPE:
      begin
         select_transfer = 1;
         
         if(~synapseType)
            precellPspRAM_wren = 1;

         if(cell_being_processed < ((1<<CELL_BITS)-1))begin
            move_to_next_cell = 1;
            next_state <= STATE_WAIT_PRE_PSP_RAM1;
         end else begin
            move_to_next_cell = 0;
            next_state <= STATE_IDLE;
         end
      end
      STATE_WAIT_PRE_PSP_RAM1:
      begin
         select_transfer = 1;
         next_state = STATE_WAIT_PRE_PSP_RAM2;
      end
      STATE_WAIT_PRE_PSP_RAM2:
      begin
         select_transfer = 1;
         next_state = STATE_TRANSFER_PRE_PSP1;
      end

      /////////////////////////////////////////////
      // verify if cell is not inside refractory
      STATE_VERIFY_REFRACTORY:
      begin
         if(refractory[dendEvent_cell])
            next_state = STATE_IDLE;
         else
            next_state = STATE_READ_PRE_PSP_AND_WEIGHT1;
      end
      
      // Fetch pre-PSP and weight
      STATE_READ_PRE_PSP_AND_WEIGHT1:
      begin
         next_state = STATE_READ_PRE_PSP_AND_WEIGHT2;
      end
      STATE_READ_PRE_PSP_AND_WEIGHT2:
      begin
         precellPspRAM_wren = 1;
         next_state = STATE_IDLE;
      end
      
      default:
      begin
         select_transfer     = 0;
         dendEventFIFO_rdreq = 0;
         cellPspRAM_wren         = 0;
         move_to_next_cell   = 0;
         precellPspRAM_wren      = 0;
         next_state = STATE_RESET;
      end
   endcase
end


// write pre-PSP during transfer phase (select_transfer) and during pre-PSP update phase (dendrite event processing)
wire [WEIGHT_BITS-1:0] new_weight = precellPspRAM_dout + weightRAM_dout;
assign precellPspRAM_wradd = select_transfer ? cell_being_processed : dendEvent_cell;
assign precellPspRAM_din = select_transfer ? 0 : new_weight;

// read pre-PSP during transfer phase (select_transfer) and during pre-PSP update phase (dendrite event processing)
assign precellPspRAM_rdadd = select_transfer ? cell_being_processed : dendEvent_cell;
assign weightRAM_rdadd = dendEventFIFO_dout;

assign cellPspRAM_wradd = cell_being_processed;
assign cellPspRAM_din   = precellPspRAM_dout;


// cycle through neuron cells to transfer prePSP -> PSP
always @(posedge rst or posedge clk)
begin
   if(rst)
      cell_being_processed <= 0;
   else if(move_to_next_cell)
      cell_being_processed <= cell_being_processed + 1;
   else if(curr_state == STATE_IDLE)
      cell_being_processed <= 0;
end

endmodule

