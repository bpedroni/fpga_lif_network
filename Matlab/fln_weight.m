function fln_weight(sp, cell, dendrite, value)
    msg = '000000000000000000000000000000000000000000000000';
    
    msg(1:3)    = '101';                    % code (3-bit code)
    msg(7:16)   = dec2bin(cell,10);         % cell (10-bit max)
    msg(17:24)  = dec2bin(dendrite,8);      % dendrite (8-bit max)
    msg(37:48)  = dec2bin(value,12);        % weight value
    
    for i=0:5
        fwrite(sp, bin2dec(msg(1+8*i:8*i+8)));
    end
end