function fln_route(sp, cell_origin, synapse, cell_destination, dendrite)
    msg = '000000000000000000000000000000000000000000000000';
    
    msg(1:3)    = '100';                        % code (3-bit code)
    msg(7:16)   = dec2bin(cell_origin,10);      % cell origin (10-bit max)
    msg(17:24)  = dec2bin(synapse,10);          % cell (10-bit max)
    msg(31:40)  = dec2bin(cell_destination,8);  % dendrite (8-bit max)
    msg(41:48)  = dec2bin(dendrite,12);         % weight value
    
    for i=0:5
        fwrite(sp, bin2dec(msg(1+8*i:8*i+8)));
    end
end