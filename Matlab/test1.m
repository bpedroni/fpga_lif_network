% connect neurons
fwrite(sp,128);
fwrite(sp,0);
fwrite(sp,0);
fwrite(sp,0);
fwrite(sp,1);
fwrite(sp,0);

fwrite(sp,128);
fwrite(sp,1);
fwrite(sp,0);
fwrite(sp,0);
fwrite(sp,0);
fwrite(sp,0);

% set neuron 0 bias=4
fwrite(sp,192);
fwrite(sp,0);           % cell
fwrite(sp,0);           % dendrite
fwrite(sp,16);          % bias code
fwrite(sp,0);
fwrite(sp,0);           % bias value

% set neuron 0 W0=64
fln_weight(sp, 0, 0, 0);
fln_weight(sp, 0, 1, 10);
fln_weight(sp, 1, 0, 100);

% turn on core 0000 spike probe
fwrite(sp,192);
fwrite(sp,0);           % cell
fwrite(sp,0);           % dendrite
fwrite(sp,128);         % spike/Vmem probe code
fwrite(sp,0);
fwrite(sp,3);           % spike/Vmem probe value

% send dendrite event
fwrite(sp,0);
fwrite(sp,0);           % cell
fwrite(sp,0);           % dendrite
pause(0.25);

fwrite(sp,192);
fwrite(sp,0);
fwrite(sp,0);
fwrite(sp,128);
fwrite(sp,0);
fwrite(sp,2);